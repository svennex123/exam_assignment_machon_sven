#pragma once
namespace svengine {
	class TextureComponent final:
		public Component
	{
	public:
		TextureComponent(std::shared_ptr<GameObject> parent,std::string filename);
		
		void Render(const Transform transform)const  override;
		void Update() override;

		~TextureComponent()=default;
		TextureComponent(const TextureComponent&) = default;
		TextureComponent(TextureComponent&&) = default;
		TextureComponent& operator=(const TextureComponent&) = default;
		TextureComponent& operator=(TextureComponent&&) = default;
	private:
		std::shared_ptr<Texture2D> _texture;
	};


}


