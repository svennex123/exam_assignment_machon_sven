#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include <SDL.h>
#include "Scene.h"
#include "TextComponent.h"
#include "TextureComponent.h"
#include "FpsComponent.h"
#include "InputComponent.h"
#include "PlayerStateComponent.h"
#include "MovementComponent.h"
#include "MovementObserver.h"

void svengine::Minigin::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	_window_width = 640;
	_window_height = 480;

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		_window_width,
		_window_height,
		SDL_WINDOW_OPENGL
	);
	if (window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(window, _window_width, _window_height);
}

/**
 * Code constructing the scene world starts here
 */
void svengine::Minigin::LoadGame() const
{
	auto& scene = SceneManager::GetInstance().CreateScene("Demo");


	auto go = std::make_shared<GameObject>("backgroundObject");
	go->AddComponent(std::make_shared<TextureComponent>(go,"background.jpg"));
	scene.Add(go);
	
	go = std::make_shared<GameObject>("logoObject");
	go->AddComponent(std::make_shared<TextureComponent>(go,"logo.png"));
	go->SetTransform(216, 180);
	scene.Add(go);

	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	
	auto to = std::make_shared<GameObject>("textObject");
	to->AddComponent(std::make_shared<TextComponent>(to,"Programming 4 Assignment", font));
	to->SetTransform(80, 20);
	scene.Add(to);

	auto fpsObject = std::make_shared<GameObject>("fpsObject");
	std::shared_ptr<TextComponent> fps_text{std::make_shared<TextComponent>(fpsObject,"test", font) };
	fpsObject->AddComponent(fps_text);

	fpsObject->AddComponent(std::make_shared<FpsComponent>(fpsObject) );

	std::shared_ptr<InputComponent> inputComponent{std::make_shared<InputComponent>(fpsObject)};
	inputComponent->AddCommand(InputEnum::key_left, std::make_shared<PlayerLeftCommand>());
	inputComponent->AddCommand(InputEnum::key_right, std::make_shared<PlayerRightCommand>());
	inputComponent->AddCommand(InputEnum::key_up, std::make_shared<PlayerUpCommand>());
	inputComponent->AddCommand(InputEnum::key_down, std::make_shared<PlayerDownCommand>());

	fpsObject->AddComponent(inputComponent);
	fpsObject->AddComponent(std::make_shared<PlayerStateComponent>(fpsObject));

	fpsObject->AddComponent(std::make_shared<MovementComponent>(fpsObject, 5,640,480));

	fpsObject->AddObserver(std::make_shared<MovementObserver>());

	fpsObject->SetTransform(0,0);
	scene.Add(fpsObject);

}

void svengine::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();
}

void svengine::Minigin::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();
	{
		auto tPrevious = std::chrono::high_resolution_clock::now();
		float lag = 0.f;
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();

		bool doContinue = true;
		while (doContinue)
		{
			auto tCurrent = std::chrono::high_resolution_clock::now();
			auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(tCurrent - tPrevious).count();
			tPrevious = tCurrent;
			lag += elapsedTime;

			doContinue = input.ProcessInput();

			while (lag >= msPerFrame)
			{
				sceneManager.FixedUpdate();
				lag -= msPerFrame;
			}

			sceneManager.Update();
			renderer.Render();

		}	
	}

	Cleanup();
}
