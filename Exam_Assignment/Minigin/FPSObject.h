#pragma once
#include "SceneObject.h"
#include "TransformComponent.h"
#include "RenderTextComponent.h"
#include "FPSComponent.h"

namespace dae {
	class FPSObject final :
		public GameObject
	{
	public:
		FPSObject(float x, float y,std::shared_ptr<Font> pFont, std::string text);
		~FPSObject();

		void Update() override;
		void Render() const override;

	private:
		TransformComponent m_transform_component_;
		RenderTextComponent m_render_text_component_;
		FPSComponent m_fps_component_;
	};
}

