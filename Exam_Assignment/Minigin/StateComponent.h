#pragma once
#include "Component.h"
namespace dae {


	class StateComponent :
		public Component
	{
	public:
		StateComponent(std::shared_ptr<GameObject>parent);
		~StateComponent();
		void SetState(PlayerState state);
		PlayerState GetState()const;
		void Render(const Transform transform) const override;
		void Update() override;

	private:
		PlayerState _current_state;
	};
}

