#pragma once

namespace svengine {
	enum class PlayerState
	{
		idle,
		moving_up,
		moving_down,
		moving_right,
		moving_left
	};

	enum class InputEnum
	{
		button_A,
		button_B,
		button_X,
		button_Y,
		key_down,
		key_left,
		key_up,
		key_right,
		joystick_down,
		joystick_up,
		joystick_left,
		joystick_right
	};

	enum class Event
	{
		move_left,
		move_up,
		move_down,
		move_right,
	};


}