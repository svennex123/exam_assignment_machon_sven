#include "MiniginPCH.h"
#include <SDL_ttf.h>
#include "TextComponent.h"
namespace svengine
{
	TextComponent::TextComponent(std::shared_ptr<GameObject> parent, std::string text, std::shared_ptr<Font>  font)
		:_text(text)
		, _font(font)
		, _needs_update(true)
		, _color(SDL_Color({ 255, 255, 255 })) // standard color of text is white
		, _texture()
		, Component(parent)
	{
	}


	void TextComponent::SetText(std::string newText)
	{
		_text = newText;
		_needs_update = true;
	}

	void TextComponent::Update()
	{
		if (_needs_update) //change the texture if text has changed
		{
			const auto surf = TTF_RenderText_Blended(_font->GetFont(), _text.c_str(), _color);
			if (surf == nullptr)
			{
				throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
			}
			auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
			if (texture == nullptr)
			{
				throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
			}
			SDL_FreeSurface(surf);


			_texture = std::make_shared<Texture2D>(texture);
			_needs_update = false;
		}


		
	}

	void TextComponent::Render(const Transform transform) const
	{
		if (_texture != nullptr)
		{
			const auto pos = transform.GetPosition();
			Renderer::GetInstance().RenderTexture(_texture->GetSDLTexture(), pos.x, pos.y);
		}
	}

	 void TextComponent::SetColor(SDL_Color color)
	{
		_color = color;
	}
}
