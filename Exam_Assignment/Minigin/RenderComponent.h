#pragma once
#include "Component.h"
#include "Texture2D.h"
#include "ResourceManager.h"
#include "TransformComponent.h"

namespace dae {
	class RenderComponent final:
		public Component
	{
	public:
		RenderComponent(std::string filename) { m_p_texture2D_ = dae::ResourceManager::GetInstance().LoadTexture(filename); }
		void Render(TransformComponent transform);

	private:
		std::shared_ptr<Texture2D> m_p_texture2D_;
	};
}

