#include "MiniginPCH.h"
#include "FPSObject.h"
#include "TransformComponent.h"
#include "RenderTextComponent.h"



dae::FPSObject::FPSObject(float x, float y, std::shared_ptr<Font> pFont, std::string text)
	:GameObject()
	,m_transform_component_(this)
	, m_render_text_component_(text, pFont, true)
	,m_fps_component_()
	
{
	m_transform_component_.SetPosition(x,y,0);
}

dae::FPSObject::~FPSObject()
{
}

void dae::FPSObject::Update()
{
	m_fps_component_.Update();
	m_render_text_component_.SetText(std::to_string(m_fps_component_.GetFPS()));
	m_render_text_component_.Update();
}

void dae::FPSObject::Render() const
{
	m_render_text_component_.Render(m_transform_component_);
}
