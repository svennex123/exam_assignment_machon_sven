#pragma once
#include "Component.h"
#include "Font.h"
#include "Texture2D.h"
#include "TransformComponent.h"

namespace dae {
	class RenderTextComponent :
		public Component
	{
	public:
		RenderTextComponent(std::string text,std::shared_ptr<Font> pFont,bool isUpdating,GameObject * object);
		void SetText(std::string newText) { m_text_ = newText; }
		std::string GetText() { return m_text_; }
		void Update();
		void Render(TransformComponent transform) const;

	private: //ADD COLOR CHANGER
		bool m_needs_update_; 
		std::string m_text_;
		std::shared_ptr<Font>  m_p_font_;
		std::shared_ptr<Texture2D> m_p_texture2D_;
	};
}

