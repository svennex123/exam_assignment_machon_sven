#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include "Component.h"
#pragma warning(pop)

namespace dae
{
	class TransformComponent final :public dae::Component
	{

		glm::vec3 mPosition;
	public:
		TransformComponent(GameObject *object):Component(object){};
		const glm::vec3& GetPosition() const { return mPosition; }
		void SetPosition(float x, float y, float z); 
	};
}
