#include "MiniginPCH.h"
#include "RenderTextComponent.h"
#include "Renderer.h"


dae::RenderTextComponent::RenderTextComponent(std::string text, std::shared_ptr<Font> pFont,bool isUpdating, GameObject * object)
	:m_text_{text}
	,m_p_font_{pFont}
	,m_p_texture2D_{nullptr}
	,m_needs_update_{isUpdating}
	,Component(object)
{
	const SDL_Color color = { 255,255,255 }; // only white text is supported now
	const auto surf = TTF_RenderText_Blended(m_p_font_->GetFont(), m_text_.c_str(), color);
	if (surf == nullptr)
	{
		throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
	}
	auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
	if (texture == nullptr)
	{
		throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
	}
	SDL_FreeSurface(surf);
	m_p_texture2D_ = std::make_shared<Texture2D>(texture);
}

void dae::RenderTextComponent::Update()
{
	if (m_needs_update_)
	{
		const SDL_Color color = { 255,255,255 }; // only white text is supported now
		const auto surf = TTF_RenderText_Blended(m_p_font_->GetFont(), m_text_.c_str(), color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_p_texture2D_ = std::make_shared<Texture2D>(texture);
	}
}

void dae::RenderTextComponent::Render(TransformComponent transform) const
{
	if (m_p_texture2D_)
	{
		const auto pos =transform.GetPosition();
		Renderer::GetInstance().RenderTexture(*m_p_texture2D_, pos.x, pos.y);
	}
}
