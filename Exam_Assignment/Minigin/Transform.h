#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec2.hpp>
#pragma warning(pop)

namespace svengine
{
	class Transform final
	{
		glm::vec2 mPosition;
	public:
		const glm::vec2& GetPosition() const { return mPosition; }
		void SetPosition(float x, float y);
	};
}
