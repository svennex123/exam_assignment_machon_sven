#pragma once
#include "SceneObject.h"
#include "Component.h"

//Base GameObject
//Holds vector with all components of the game object
// iterates over them all and if they're active updates/renders them

namespace dae
{

	class GameObject_N :
		public SceneObject
	{
	public:
		GameObject_N():_components_{}{}
		~GameObject_N();

		void Update() override;
		void Render() const override;
		void AddComponent(Component * component) { _components_.push_back(component); };
		void SetTransform(float x, float y) { _transform.SetPosition(x, y); }
	private:

		std::vector<Component*> _components_;
		Transform _transform;
	};

	inline GameObject_N::~GameObject_N()
	{
		uint64_t size{ _components_.size() };
		for (int i = 0; i < size; i++)
		{
			delete _components_[size-i-1];
			_components_.pop_back();
		}
	}

	inline void GameObject_N::Update()
	{
		for (auto component : _components_)
		{
			if(component->GetActive())
			component->Update();
		}
		
	}

	inline void GameObject_N::Render() const
	{
		for (auto component : _components_)
		{
			if(component->GetActive())
			component->Render(_transform);
		}
	}
}
