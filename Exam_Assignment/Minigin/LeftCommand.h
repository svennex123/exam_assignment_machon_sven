#pragma once
#include "Command.h"

namespace dae {
	class LeftCommand :
		public Command
	{
	public:
		LeftCommand();
		~LeftCommand();
		void Execute(std::shared_ptr<GameObject> pobject) override;
	};



	LeftCommand::LeftCommand()
		:Command()
	{
	}


	LeftCommand::~LeftCommand()
	{
	}

	inline void LeftCommand::Execute(std::shared_ptr<GameObject> gameObject)
	{
		auto transform =gameObject->GetTransformRef().GetPosition();

		gameObject->SetTransform(transform.x - 5, transform.y);

	}
}
