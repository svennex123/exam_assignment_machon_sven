#pragma once
#include "PlayerStateComponent.h"
#include "MovementComponent.h"

namespace svengine {
	class Command
	{
	public:
		Command() = default;
		virtual ~Command() = default;
		virtual void Execute(std::shared_ptr<GameObject> pobject) = 0;
	};

	//
	//LEFT COMMAND
	//
	class PlayerLeftCommand final :
		public Command
	{
	public:
		PlayerLeftCommand() = default;
		~PlayerLeftCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();
			if (state.get())
			{
				if (state->GetState() != PlayerState::moving_right&&
					state->GetState() != PlayerState::moving_down&&
					state->GetState() != PlayerState::moving_up)
				{
					state->SetState(PlayerState::moving_left);
					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveLeft();
				}
			}

		}
	};

	//
	//RIGHT COMMAND
	//
	class PlayerRightCommand final :
		public Command
	{
	public:
		PlayerRightCommand() = default;
		~PlayerRightCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here

			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if (state->GetState() != PlayerState::moving_left&&
					state->GetState() != PlayerState::moving_down&&
					state->GetState() != PlayerState::moving_up)
				{
					state->SetState(PlayerState::moving_right);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveRight();
				}
			}
		}
	};

	//
	//UP COMMAND
	//
	class PlayerUpCommand final :
		public Command
	{
	public:
		PlayerUpCommand() = default;
		~PlayerUpCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if (state->GetState() != PlayerState::moving_down&&
					state->GetState() != PlayerState::moving_right&&
					state->GetState() != PlayerState::moving_left)
				{
					state->SetState(PlayerState::moving_up);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveUp();
				}
			}
		}
	};

	//
	//DOWN COMMAND
	//
	class PlayerDownCommand final :
		public Command
	{
	public:
		PlayerDownCommand() = default;
		~PlayerDownCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if (state->GetState() != PlayerState::moving_up&&
					state->GetState() != PlayerState::moving_left&&
					state->GetState() != PlayerState::moving_right)
				{
					state->SetState(PlayerState::moving_down);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveDown();
				}
			}
		}
	};


}

