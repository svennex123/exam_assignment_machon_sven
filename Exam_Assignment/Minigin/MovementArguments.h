#pragma once
#include "ObserverArguments.h"
namespace svengine {
	class MovementArguments :
		public ObserverArguments
	{
	public:
		MovementArguments(int speed);
		
		const int GetSpeed()const;

		~MovementArguments() = default;
		MovementArguments(const MovementArguments&) = default;
		MovementArguments(MovementArguments&&) = default;
		MovementArguments& operator=(const MovementArguments&) = default;
		MovementArguments& operator = (MovementArguments&&) = default;


	private:
		int _speed;

	};

}