#include "MiniginPCH.h"
#include "InputComponent.h"
namespace svengine
{
	InputComponent::InputComponent(std::shared_ptr<GameObject> parent)
		:_commands{}
		, Component(parent)
	{
	}

	void InputComponent::Update()
	{
		HandleInput();
	}

	void InputComponent::Render(const Transform) const
	{
	}

	void InputComponent::HandleInput()
	{
		InputManager & inputManager{ InputManager::GetInstance() };
		_parent.lock()->GetComponent<PlayerStateComponent>()->SetState(PlayerState::idle);
		for (auto command : _commands)
		{
			if (inputManager.IsPressed(command.first))
				command.second->Execute(_parent.lock());
		}
	}

	void InputComponent::AddCommand(InputEnum key, std::shared_ptr<Command> command)
	{
		_commands.insert(std::pair<InputEnum, std::shared_ptr<Command>>(key, command));
	}
}
