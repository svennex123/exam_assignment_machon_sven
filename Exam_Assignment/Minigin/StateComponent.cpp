#include "MiniginPCH.h"
#include "StateComponent.h"

namespace dae {
	StateComponent::StateComponent(std::shared_ptr<GameObject>parent)
		:Component(parent)
		,_current_state(PlayerState::idle)
	{
	}


	StateComponent::~StateComponent()
	{
	}

	void StateComponent::SetState(PlayerState state)
	{
		_current_state = state;
	}

	PlayerState StateComponent::GetState() const
	{
		return _current_state;
	}

	void StateComponent::Render(const Transform ) const
	{
	}

	void StateComponent::Update()
	{
	}
}
