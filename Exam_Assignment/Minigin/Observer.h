#pragma once
namespace svengine {
	class ObserverArguments;

	class Observer
	{
	public:
		inline Observer();
		virtual ~Observer()=default;
		virtual void OnNotify(GameObject * object, Event event,ObserverArguments * argument) = 0;
	};

	inline Observer::Observer()
	{
	}

}
