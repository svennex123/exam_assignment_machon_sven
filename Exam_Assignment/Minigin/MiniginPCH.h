#pragma once

#include "targetver.h"

#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <tchar.h>
#include <memory> // smart pointers
#include <vector>



#define WIN32_LEAN_AND_MEAN
#include <windows.h>


#include "Enums.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"


#include "Transform.h"
#include "Log.h" // Various logging functions
#include "Component.h"
#include "GameObject.h"
#include "Observer.h"
#include "ObserverArguments.h"

#include "Command.h"
