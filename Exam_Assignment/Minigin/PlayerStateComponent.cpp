#include "MiniginPCH.h"
#include "PlayerStateComponent.h"

namespace svengine {
	PlayerStateComponent::PlayerStateComponent(std::shared_ptr<GameObject>parent)
		:Component(parent)
		,_current_state(PlayerState::idle)
	{
	}

	void PlayerStateComponent::SetState(PlayerState state)
	{
		_current_state = state;
	}

	PlayerState PlayerStateComponent::GetState() const
	{
		return _current_state;
	}

	void PlayerStateComponent::Render(const Transform ) const
	{
	}

	void PlayerStateComponent::Update()
	{
	}
}
