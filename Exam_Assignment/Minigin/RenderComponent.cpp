#include "MiniginPCH.h"
#include "RenderComponent.h"
#include "Renderer.h"


void dae::RenderComponent::Render(TransformComponent transform)
{
	const auto pos = transform.GetPosition();
	dae::Renderer::GetInstance().RenderTexture(*m_p_texture2D_, pos.x, pos.y);
}
