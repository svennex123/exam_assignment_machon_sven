#include "MiniginPCH.h"
#include "Texture2D.h"
#include "TextureComponent.h"
namespace svengine
{
	 TextureComponent::TextureComponent(std::shared_ptr<GameObject> parent, std::string filename)
		:Component(parent)
	{
		_texture = ResourceManager::GetInstance().LoadTexture(filename);

	}

	void TextureComponent::Render(const Transform transform) const
	{
		if (_texture != nullptr)
		{
			const auto pos = transform.GetPosition();
			Renderer::GetInstance().RenderTexture(_texture->GetSDLTexture(), pos.x, pos.y);
		}
	}

	 void TextureComponent::Update()
	{
	}
}
