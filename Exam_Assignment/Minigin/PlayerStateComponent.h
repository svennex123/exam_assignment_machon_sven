#pragma once
#include "Component.h"
namespace svengine {


	class PlayerStateComponent final:
		public Component
	{
	public:
		PlayerStateComponent(std::shared_ptr<GameObject>parent);
		void SetState(PlayerState state);
		PlayerState GetState()const;
		void Render(const Transform transform) const override;
		void Update() override;

		~PlayerStateComponent() = default;
		PlayerStateComponent(const PlayerStateComponent&) = default;
		PlayerStateComponent(PlayerStateComponent&&) = default;
		PlayerStateComponent& operator=(const PlayerStateComponent&) = default;
		PlayerStateComponent& operator=(PlayerStateComponent&&) = default;

	private:
		PlayerState _current_state;
	};
}

