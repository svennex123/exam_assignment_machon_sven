#pragma once
#include <chrono>

namespace svengine {
	class TextComponent;
	class FpsComponent final:
		public Component
	{
	public:
		FpsComponent(std::shared_ptr<GameObject> parent);
		void Update() override;
		void Render(const Transform transform) const override;

		~FpsComponent() = default;
		FpsComponent(const FpsComponent&) = default;
		FpsComponent(FpsComponent&&) = default;
		FpsComponent& operator = (const FpsComponent&) = default;
		FpsComponent& operator=(FpsComponent&&) = default;

	private:
		std::shared_ptr<TextComponent> _ptext_component;
		std::chrono::steady_clock::time_point _current_time;
		std::chrono::steady_clock::time_point _previous_time;

	};




}


