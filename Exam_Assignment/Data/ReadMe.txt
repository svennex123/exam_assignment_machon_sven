This is the engine that I made for the prog 4 exam.

I have applied the component, command,state and observer pattern as seen in class.

the core idea behind my engine is that a scene consists out of an array of game objects. Each game objects holds a vector to the components that are part of it. each component then has a pointer to their owning object through which it can get pointers to other necessary components on the object.
each scene holds a level object through which the level gets created and all the objects spawned in the right location.
The level gets build based on a csv file contructed with the program tiled.
to show that the engine works I made DigDug in the framework of my engine.

to decide wether solo or co op get played, define a macro in minigin.cpp

When in game press r to restart

there is at the moment no Versus mode.

https://bitbucket.org/svennex123/exam_assignment/src/master/