#pragma once
#include "Component.h"
#include "PlayerTextureComponent.h"

class HoseComponent final:
	public svengine::Component
{
public:
	HoseComponent(std::shared_ptr<svengine::GameObject> parent);


	void Update() override;
	void Render(const svengine::Transform) const override;
	bool Shoot(svengine::PlayerState state);
	void SetActive(bool active) override;

	~HoseComponent() = default;
	HoseComponent(const HoseComponent&) = default;
	HoseComponent(HoseComponent&&) = default;
	HoseComponent& operator=(const HoseComponent&) = default;
	HoseComponent& operator=(HoseComponent&&) = default;

private:
	
	svengine::Transform _transform;
	float _life_time;
	float _current_time;
	int _width, _height;
	std::map < svengine::PlayerState, std::shared_ptr<svengine::Texture2D>> _textures;
	svengine::PlayerState _current_state;
	

};

