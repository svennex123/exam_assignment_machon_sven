#include "MiniginPCH.h"
#include "Scene.h"
#include "SceneObject.h"

unsigned int svengine::Scene::idCounter = 0;

svengine::Scene::Scene(const std::string& name) : mName(name) {}

svengine::Scene::~Scene() = default;

void svengine::Scene::Add(const std::shared_ptr<SceneObject>& object)
{
	mObjects.push_back(object);
}

void svengine::Scene::Update()
{
	for(auto gameObject : mObjects)
	{
		gameObject->Update();
	}
}

void svengine::Scene::FixedUpdate()
{
	for (auto gameObject : mObjects)
	{
		gameObject->FixedUpdate();
	}
}

void svengine::Scene::Render() const
{
	for (const auto gameObject : mObjects)
	{
		gameObject->Render();
	}
}

std::vector<std::shared_ptr<svengine::GameObject>>svengine::Scene::GetObjectsOfName(std::string name)
{
	std::vector<std::shared_ptr<svengine::GameObject>> objects;
	for(auto sceneObject:mObjects)
	{
		if (std::dynamic_pointer_cast<GameObject>(sceneObject)->GetName() == name)
			objects.push_back(std::dynamic_pointer_cast<GameObject>(sceneObject));
	}
	return objects;
}

