#include "MiniginPCH.h"
#include "MovementObserver.h"
#include "MovementArguments.h"

namespace svengine {
	MovementObserver::MovementObserver()
		:Observer(),_amount_moved{}
	{
	}

	void MovementObserver::OnNotify(GameObject* object, Event event, ObserverArguments * arguments)
	{
		if (object->GetName() == "fpsObject")
		{
			if (event == Event::move_down)
			{
				_amount_moved+=static_cast<MovementArguments*>(arguments)->GetSpeed();
				
			}
		}
	}
}
