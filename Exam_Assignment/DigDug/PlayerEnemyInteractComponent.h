#pragma once
#include "Component.h"
class PlayerEnemyInteractComponent final:
	public svengine::Component
{
public:
	PlayerEnemyInteractComponent(std::shared_ptr<svengine::GameObject> parent);

	void AddColliders(std::weak_ptr<svengine::GameObject> collider);
	void Update() override;
	std::vector<std::weak_ptr<svengine::GameObject>>GetCollidersInArea(svengine::Transform, int width, int height);

	~PlayerEnemyInteractComponent() = default;
	PlayerEnemyInteractComponent(const PlayerEnemyInteractComponent&) = default;
	PlayerEnemyInteractComponent(PlayerEnemyInteractComponent&&) = default;
	PlayerEnemyInteractComponent& operator=(const PlayerEnemyInteractComponent&) = default;
	PlayerEnemyInteractComponent& operator=(PlayerEnemyInteractComponent&&) = default;
private:
	svengine::Transform * _transform;
	std::vector<std::weak_ptr<svengine::GameObject>> _colliders;


};

