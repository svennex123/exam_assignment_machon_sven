#pragma once
#include <map>
#include "Command.h"

namespace svengine {
	class InputComponent final:
		public Component
	{
	public:
		InputComponent(std::shared_ptr<GameObject> parent);
		void Update() override;
		void HandleInput();
		void AddCommand(InputEnum key, std::shared_ptr<Command> command);

		~InputComponent() = default;
		InputComponent(const InputComponent&) = default;
		InputComponent(InputComponent&&) = default;
		InputComponent& operator=(const InputComponent&) = default;
		InputComponent& operator=(InputComponent&&) = default;

	private:
		std::map<InputEnum,std::shared_ptr<Command>> _commands;
	};

	

}

