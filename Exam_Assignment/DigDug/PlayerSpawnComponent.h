#pragma once
#include "Component.h"

class PlayerSpawnComponent final:
	public svengine::Component
{
public:
	PlayerSpawnComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level, svengine::Scene* scene);


	void Spawn(std::shared_ptr<svengine::Observer> observer);

	~PlayerSpawnComponent() = default;
	PlayerSpawnComponent(const PlayerSpawnComponent&) = default;
	PlayerSpawnComponent(PlayerSpawnComponent&&) = default;
	PlayerSpawnComponent& operator=(const PlayerSpawnComponent&) = default;
	PlayerSpawnComponent& operator=(PlayerSpawnComponent&&) = default;

private:

	std::weak_ptr<LevelComponent> _level;
	svengine::Scene* _scene;
	
};

