#pragma once
#include "Component.h"
class Texture2D;

class LevelComponent final :
	public svengine::Component
{
public:
	LevelComponent(std::shared_ptr<svengine::GameObject>parent,std::string path);	
	void LoadLevel(std::string file, std::vector<std::string> texturefiles);
	void Render(const svengine::Transform transform) const override;

	const std::vector<svengine::Block*> GetBlocks() { return _blocks; }
	const std::vector<svengine::Block*> GetPookaSpawns() { return _pooka_spawns; }
	const std::vector<svengine::Block*> GetBoulderSpawns() { return _boulder_spawns; }
	const std::vector<svengine::Block*> GetPlayerSpawns() { return _player_spawns; }
	const std::vector<svengine::Block*> GetFygarSpawns() { return _fygar_spawns; }

	void Dig(int idx);

	std::pair<int, int> GetRowsAndColumns() { return{ _rows,_columns }; }
	int GetBlockSize() { return _blocks[0]->height; }

	~LevelComponent();
	LevelComponent(const LevelComponent&) = default;
	LevelComponent(LevelComponent&&) = default;
	LevelComponent& operator=(const LevelComponent&) = default;
	LevelComponent& operator=(LevelComponent&&) = default;



private:
	std::vector<svengine::Block*>_blocks;
	std::vector<svengine::Block*> _pooka_spawns;
	std::vector<svengine::Block*> _boulder_spawns;
	std::vector<svengine::Block*> _player_spawns;
	std::vector<svengine::Block*> _fygar_spawns;

	std::string _data_path;
	std::shared_ptr<svengine::Texture2D> _dig_texture;

	int _rows, _columns;
};

