#include "MiniginPCH.h"
#include "PlayerSpawnComponent.h"
#include "PlayerEnemyInteractComponent.h"
#include "LivesComponent.h"
#include "LevelTrackComponent.h"
#include "InputComponent.h"
#include "Scene.h"


PlayerSpawnComponent::PlayerSpawnComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level, svengine::Scene* scene)
	:Component(parent)
	,_level(level)
	,_scene(scene)
{
}


void PlayerSpawnComponent::Spawn( std::shared_ptr<svengine::Observer> observer)
{
	int nummer{ 0 };
	for (auto spawnPoint : _level.lock()->GetPlayerSpawns()) {
		nummer++;
		std::shared_ptr<svengine::GameObject> DigDug{ std::make_shared<svengine::GameObject>("DigDug") };

		std::shared_ptr<PlayerTextureComponent> textures{ std::make_shared<PlayerTextureComponent>(DigDug) };
		textures->AddTexture(svengine::PlayerState::moving_down, "DigDugDown.png");
		textures->AddTexture(svengine::PlayerState::moving_left, "DigDugLeft.png");
		textures->AddTexture(svengine::PlayerState::moving_right, "DigDugRight.png");
		textures->AddTexture(svengine::PlayerState::moving_up, "DigDugUp.png");

		std::shared_ptr<svengine::InputComponent> inputComponent{ std::make_shared<svengine::InputComponent>(DigDug) };

		if (nummer==1)
		{
			
		inputComponent->AddCommand(svengine::InputEnum::key_A, std::make_shared<svengine::PlayerLeftCommand>());
		inputComponent->AddCommand(svengine::InputEnum::key_D, std::make_shared<svengine::PlayerRightCommand>());
		inputComponent->AddCommand(svengine::InputEnum::key_W, std::make_shared<svengine::PlayerUpCommand>());
		inputComponent->AddCommand(svengine::InputEnum::key_S, std::make_shared<svengine::PlayerDownCommand>());
		inputComponent->AddCommand(svengine::InputEnum::key_Q, std::make_shared<svengine::ShootCommand>());
		}
		else
		{
			inputComponent->AddCommand(svengine::InputEnum::key_left, std::make_shared<svengine::PlayerLeftCommand>());
			inputComponent->AddCommand(svengine::InputEnum::key_right, std::make_shared<svengine::PlayerRightCommand>());
			inputComponent->AddCommand(svengine::InputEnum::key_up, std::make_shared<svengine::PlayerUpCommand>());
			inputComponent->AddCommand(svengine::InputEnum::key_down, std::make_shared<svengine::PlayerDownCommand>());
			inputComponent->AddCommand(svengine::InputEnum::key_slash, std::make_shared<svengine::ShootCommand>());

		}

		DigDug->AddComponent(textures);

		DigDug->AddComponent(inputComponent);
		DigDug->AddComponent(std::make_shared<svengine::PlayerStateComponent>(DigDug));
		DigDug->AddComponent(std::make_shared<svengine::MovementComponent>(DigDug, 1, 406, 602));
		DigDug->AddComponent(std::make_shared<DigComponent>(DigDug, _level));
		DigDug->AddComponent(std::make_shared<LevelTrackComponent>(DigDug, _level));

		DigDug->AddComponent(std::make_shared<LivesComponent>(DigDug, 3,int(spawnPoint->transform.GetPosition().x),int(spawnPoint->transform.GetPosition().y)));
		auto collisionComp{ std::make_shared<PlayerEnemyInteractComponent>(DigDug) };
		DigDug->AddComponent(std::make_shared<HoseComponent>(DigDug));

		for (auto collider : _scene->GetObjectsOfName("pooka"))
			collisionComp->AddColliders(collider);

		for (auto collider : _scene->GetObjectsOfName("fygar"))
			collisionComp->AddColliders(collider);

		DigDug->AddComponent(collisionComp);


		DigDug->SetTransform(spawnPoint->transform.GetPosition().x, spawnPoint->transform.GetPosition().y-7);


		DigDug->AddObserver(observer);
		_scene->Add(DigDug);
	}
}
