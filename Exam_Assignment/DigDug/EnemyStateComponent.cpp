#include "MiniginPCH.h"
#include "EnemyStateComponent.h"


EnemyStateComponent::EnemyStateComponent(std::shared_ptr<svengine::GameObject> parent)
	:Component(parent)
	,_current_state(svengine::EnemyState::moving_down)
{
}

