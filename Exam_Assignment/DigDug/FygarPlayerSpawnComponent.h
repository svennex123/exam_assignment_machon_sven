#pragma once
#include "Component.h"
class FygarPlayerSpawnComponent :
	public svengine::Component
{
public:
	FygarPlayerSpawnComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level, svengine::Scene* scene, std::shared_ptr<svengine::Observer> observer);
	~FygarPlayerSpawnComponent();
	void Spawn();

private:
	svengine::Scene * _scene;
	std::weak_ptr<LevelComponent> _level;
	std::shared_ptr<svengine::Observer> _observer;

};

