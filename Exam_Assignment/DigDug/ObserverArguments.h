#pragma once

namespace svengine {
	class ObserverArguments
	{
	public:
		inline ObserverArguments();
		virtual ~ObserverArguments()=default;
	};

	inline ObserverArguments::ObserverArguments()
	{
	}


}
