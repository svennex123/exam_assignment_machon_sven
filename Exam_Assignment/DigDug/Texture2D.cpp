#include "MiniginPCH.h"
#include "Texture2D.h"
#include <SDL.h>

svengine::Texture2D::~Texture2D()
{
	SDL_DestroyTexture(mTexture);
}

SDL_Texture* svengine::Texture2D::GetSDLTexture() const
{
	return mTexture;
}

svengine::Texture2D::Texture2D(SDL_Texture* texture)
{
	mTexture = texture;
}
