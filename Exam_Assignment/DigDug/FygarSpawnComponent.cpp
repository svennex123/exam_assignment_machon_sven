#include "MiniginPCH.h"
#include "FygarSpawnComponent.h"
#include "LevelTrackComponent.h"
#include "EnemyLifeComponent.h"
#include "Scene.h"
#include "PlayerEnemyInteractComponent.h"
#include "FireBreathComponent.h"


FygarSpawnComponent::FygarSpawnComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level, svengine::Scene* scene, std::shared_ptr<svengine::Observer> observer)
	:Component(parent)
	, _level(level)
	, _scene(scene)
	, _observer(observer)
{
}

void FygarSpawnComponent::Spawn()
{
	std::vector<svengine::Block*> spawnPoints{ _level.lock()->GetFygarSpawns() };

	for (auto spawnPoint : spawnPoints)
	{
		std::shared_ptr<svengine::GameObject> fygar{ std::make_shared<svengine::GameObject>("fygar") };


		fygar->AddComponent(std::make_shared<svengine::TextureComponent>(fygar, "fygar.png"));
		
		fygar->AddComponent(std::make_shared<EnemyStateComponent>(fygar));
		
		fygar->AddComponent(
			std::make_shared<svengine::MovementComponent>
			(fygar, 1, _level.lock()->GetRowsAndColumns().second*_level.lock()->GetBlockSize(), _level.lock()->GetRowsAndColumns().first*_level.lock()->GetBlockSize()));
		
		std::shared_ptr<EnemyControlComponent> control_component{ std::make_shared<EnemyControlComponent>(fygar,_level) };
		control_component->AddCommand(svengine::EnemyState::moving_down, std::make_shared<svengine::EnemyDownCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_up, std::make_shared<svengine::EnemyUpCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_left, std::make_shared<svengine::EnemyLeftCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_right, std::make_shared<svengine::EnemyRightCommand>());
		fygar->AddComponent(control_component);

		fygar->AddComponent(std::make_shared<EnemyLifeComponent>(fygar));

		fygar->AddComponent(std::make_shared<LevelTrackComponent>(fygar, _level));

		auto collisionComp{ std::make_shared<PlayerEnemyInteractComponent>(fygar) };
		for (auto collider : _scene->GetObjectsOfName("DigDug"))
			collisionComp->AddColliders(collider);
		fygar->AddComponent(collisionComp);
		collisionComp->SetActive(false);

		fygar->AddComponent(std::make_shared<FireBreathComponent>(fygar));

		fygar->SetTransform(spawnPoint->transform.GetPosition().x, spawnPoint->transform.GetPosition().y);

		fygar->AddObserver(_observer);

		_scene->Add(fygar);
	}
}
