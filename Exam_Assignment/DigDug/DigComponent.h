#pragma once
#include "Component.h"
#include "LevelComponent.h"

class DigComponent final:
	public svengine::Component
{
public:
	DigComponent(std::shared_ptr<svengine::GameObject>parent,std::weak_ptr<LevelComponent> level);

	void Render(const svengine::Transform) const override {};
	void Dig();

	~DigComponent() = default;
	DigComponent(const DigComponent&) = default;
	DigComponent(DigComponent&&) = default;
	DigComponent& operator=(const DigComponent&) = default;
	DigComponent& operator=(DigComponent&&) = default;

private:
	std::weak_ptr<LevelComponent> _level;
	int _x, _y, _width, _height;
	svengine::Transform * _transform;

	void DigHelper(std::vector<svengine::Block*> blocks, int idx);

};

