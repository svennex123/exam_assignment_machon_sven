#include "MiniginPCH.h"
#include "DigComponent.h"
#include "PlayerTextureComponent.h"

#include "LevelTrackComponent.h"


DigComponent::DigComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level)
	:Component(parent)
	,_level(level)
{
	_transform = &_parent.lock()->GetTransformRef();

	std::shared_ptr<PlayerTextureComponent> texture{ parent->GetComponent<PlayerTextureComponent>() };

	if (texture)
	{
		_width = texture->GetWidthAndHeight().first;
		_height = texture->GetWidthAndHeight().second;
	}
}

void DigComponent::Dig()
{
	auto blocks{ _level.lock()->GetBlocks() };
	
	std::pair<int, int> Dimensions{ _parent.lock()->GetComponent<LevelTrackComponent>()->GetRowsAndColumns() };

	_x = int(_transform->GetPosition().x);
	_y = int(_transform->GetPosition().y);

	int idx=_parent.lock()->GetComponent<LevelTrackComponent>()->GetGridIndex();

	DigHelper(blocks, idx);

	DigHelper(blocks,idx + 1*Dimensions.second);

	DigHelper(blocks, idx+ 2*Dimensions.second);

	DigHelper(blocks, idx - 1*Dimensions.second);


	DigHelper(blocks, idx+1);

	DigHelper(blocks, idx+1 + 1*Dimensions.second);

	DigHelper(blocks,idx+1+ 2*Dimensions.second);
		
	DigHelper(blocks,idx+1- 1*Dimensions.second);



	DigHelper(blocks, idx+2);

	DigHelper(blocks,idx+2+ 1*Dimensions.second);

	DigHelper(blocks, idx+2 + 2*Dimensions.second);

	DigHelper(blocks, idx+2- 1*Dimensions.second);


	DigHelper(blocks, idx -1);

	DigHelper(blocks, idx -1 + 1 * Dimensions.second);

	DigHelper(blocks, idx -1 + 2 * Dimensions.second);

	DigHelper(blocks, idx -1 - 1 * Dimensions.second);
}

void DigComponent::DigHelper(std::vector<svengine::Block*> blocks, int idx)
{
	std::pair<int, int> Dimensions{ _level.lock()->GetRowsAndColumns() };

	if ((idx >= 0 && idx < Dimensions.first*Dimensions.second) && !blocks[idx]->IsDug)
	{
		
			auto blockPosition{ blocks[idx]->transform.GetPosition() };
			if (_x <= blockPosition.x + blocks[idx]->width &&
				_x + _width >= blockPosition.x &&
				_y + _height >= blockPosition.y  &&
				_y <= blockPosition.y + blocks[idx]->height)
			
				_level.lock()->Dig(int(idx));
	}
}
