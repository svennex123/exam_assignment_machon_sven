#pragma once
#include "Component.h"
#include <map>

namespace svengine
{
class Command;
}


class EnemyControlComponent final:
	public svengine::Component
{
public:
	EnemyControlComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent> level);
	void Update() override;
	void AddCommand(svengine::EnemyState state, std::shared_ptr<svengine::Command>command);
	bool MovementWasValid();

	~EnemyControlComponent()=default;
	EnemyControlComponent(const EnemyControlComponent &) = default;
	EnemyControlComponent(EnemyControlComponent&& ) = default;
	EnemyControlComponent& operator=(const EnemyControlComponent&other);
	EnemyControlComponent& operator=(EnemyControlComponent&&) = default;
	 


private:
	std::map<svengine::EnemyState, std::shared_ptr<svengine::Command>> _commands;
	std::weak_ptr<LevelComponent> _level;
	int _x, _y, _width, _height;
	svengine::Transform * _transform;


	bool MovementHelper( std::vector<svengine::Block*>blocks, int idx);
};

