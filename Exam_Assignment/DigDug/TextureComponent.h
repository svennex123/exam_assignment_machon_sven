#pragma once
namespace svengine {
	class TextureComponent final:
		public Component
	{
	public:
		TextureComponent(std::shared_ptr<GameObject> parent,std::string filename);
		TextureComponent(std::shared_ptr<GameObject>parent, std::shared_ptr<Texture2D>texture);
		
		void Render(const Transform transform)const  override;
		std::pair<int, int> GetWidthAndHeight()const;



		~TextureComponent()=default;
		TextureComponent(const TextureComponent&) = default;
		TextureComponent(TextureComponent&&) = default;
		TextureComponent& operator=(const TextureComponent&) = default;
		TextureComponent& operator=(TextureComponent&&) = default;
	private:
		std::shared_ptr<Texture2D> _texture;
	};


}


