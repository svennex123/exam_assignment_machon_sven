#include "MiniginPCH.h"
#include "MovementComponent.h"
#include "MovementArguments.h"

namespace svengine {
	MovementComponent::MovementComponent(std::shared_ptr<GameObject> parent,int speed,int xLimit,int yLimit)
		:Component(parent)
		, _transform(&_parent.lock()->GetTransformRef())
		,_speed(speed)
		,_x_limit(xLimit)
		,_y_limit(yLimit)
	{
	}

	bool MovementComponent::MoveLeft()const
	{
		if (_transform->GetPosition().x-_speed>0)
		{
		_transform->SetPosition(_transform->GetPosition().x - _speed, _transform->GetPosition().y);
		return true;
		}


		return false;
		
	}

	bool MovementComponent::MoveRight()const
	{
		if(_transform->GetPosition().x+_speed+14<_x_limit)
		{
		_transform->SetPosition(_transform->GetPosition().x + _speed, _transform->GetPosition().y);
		return true;
		}
		return false;
	}

	bool MovementComponent::MoveUp()const
	{
		if (_transform->GetPosition().y-_speed>0)
		{
		_transform->SetPosition(_transform->GetPosition().x , _transform->GetPosition().y-_speed);
		return true;
		}

		return false;
	}

	bool MovementComponent::MoveDown()const
	{

		if(_transform->GetPosition().y+_speed+14<_y_limit)
		{
		_transform->SetPosition(_transform->GetPosition().x , _transform->GetPosition().y+_speed);
		return true;
		}
		return false;
	}

}
