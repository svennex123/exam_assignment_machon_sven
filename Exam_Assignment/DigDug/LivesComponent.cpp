#include "MiniginPCH.h"
#include "LivesComponent.h"


LivesComponent::LivesComponent(std::shared_ptr<svengine::GameObject> parent, int amntLives,int x,int y)
	:Component(parent)
	,_current_lives(amntLives)
	,_x(x)
	,_y(y)
{
}


void LivesComponent::OnHit()
{
	svengine::ObserverArguments arg{};
	_parent.lock()->Notify(svengine::Event::got_killed,& arg);
	_current_lives--;

	if (_current_lives > 0)
		_parent.lock()->SetTransform(float(_x),float(_y));

	else
	{
		_parent.lock()->SetActive(false);
	}


}
