#pragma once

struct SDL_Window;

namespace svengine
{
	class Minigin
	{
		const int msPerFrame = 16; //16 for 60 fps, 33 for 30 fps
		SDL_Window* window{};
	public:
		void Initialize();
		void LoadGame() const;
		void Cleanup();
		void Run();

	private:
		void LoadLevel1()const;
		void LoadLevel2()const;
		void LoadCoOp1()const;
		void LoadCoOp2()const;
		int _window_width, _window_height;
	};
}