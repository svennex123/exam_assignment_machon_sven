#pragma once
#include "Component.h"
class LevelTrackComponent final:
	public svengine::Component
{
public:
	LevelTrackComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent>level);

	void Update() override;

	int GetGridIndex()const { return _idx; }
	std::vector<svengine::Block*> GetBlocks()const { return _level.lock()->GetBlocks(); }
	std::pair<int, int> GetRowsAndColumns()const { return _level.lock()->GetRowsAndColumns(); }


	~LevelTrackComponent() = default;
	LevelTrackComponent(const LevelTrackComponent&) = default;
	LevelTrackComponent(LevelTrackComponent&&) = default;
	LevelTrackComponent& operator=(const LevelTrackComponent&) = default;
	LevelTrackComponent& operator=(LevelTrackComponent&&) = default;
private:
	std::weak_ptr<LevelComponent> _level;
	int _x, _y, _width, _height;
	svengine::Transform * _transform;
	int _idx;
};

