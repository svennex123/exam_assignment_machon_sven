#include "MiniginPCH.h"
#include "TextureListComponent.h"


TextureListComponent::TextureListComponent(std::shared_ptr<svengine::GameObject>parent)
	:Component(parent)
{
}


void TextureListComponent::Render(const svengine::Transform) const
{
	for(auto texture:_textures)
	{
		if (texture.first != nullptr)
		{
			const auto pos = texture.second.GetPosition();
			svengine::Renderer::GetInstance().RenderTexture(texture.first->GetSDLTexture(), pos.x, pos.y);
		}
	}
}

void TextureListComponent::AddTexture(std::shared_ptr<svengine::Texture2D> texture,int x,int y)
{
	svengine::Transform transform{};
	transform.SetPosition(float(x), float(y));

	_textures.push_back(std::pair<std::shared_ptr<svengine::Texture2D>,svengine::Transform>(texture,transform));
	
}

void TextureListComponent::RemoveTexture()
{
	_textures.pop_back();
}
