#pragma once
#include "Component.h"
class Texture2D;

class TextureListComponent final :
	public svengine::Component
{
public:
	TextureListComponent(std::shared_ptr<svengine::GameObject>parent);
	
	void Render(const svengine::Transform) const override;

	void AddTexture(std::shared_ptr<svengine::Texture2D> texture, int x,int y);
	void RemoveTexture();

	~TextureListComponent()=default;
	TextureListComponent(const TextureListComponent&) = default;
	TextureListComponent(TextureListComponent&&) = default;
	TextureListComponent& operator=(const TextureListComponent&) = default;
	TextureListComponent& operator=(TextureListComponent&&) = default;
private:
	std::vector<std::pair<std::shared_ptr<svengine::Texture2D>,svengine::Transform>> _textures;
	
};

