#pragma once
#include "Component.h"
#include "GameObject.h"

class LivesComponent final:
	public svengine::Component
{
public:
	LivesComponent(std::shared_ptr<svengine::GameObject> parent, int amntLives,int x,int y);


	void OnHit();
	const int GetLives()const { return _current_lives; }


	~LivesComponent() = default;
	LivesComponent(const LivesComponent&) = default;
	LivesComponent(LivesComponent&&) = default;
	LivesComponent& operator=(const LivesComponent&) = default;
	LivesComponent& operator=(LivesComponent&&) = default;
private:
	int _current_lives;
	int _x, _y;
};

