#include "MiniginPCH.h"
#include "LevelComponent.h"
#include <fstream>
#include "Transform.h"
#include "Texture2D.h"
#include "Renderer.h"

LevelComponent::LevelComponent(std::shared_ptr<svengine::GameObject>parent,std::string path)
	:Component(parent)
	,_data_path(path)
	,_rows()
	,_columns()
{
	_dig_texture = svengine::ResourceManager::GetInstance().LoadTexture(_data_path + "black.png");
}


LevelComponent::~LevelComponent()
{
	size_t size{ _blocks.size() };
	for (size_t i{};i<size;i++)
	{
		delete _blocks[size - i-1];
		_blocks.pop_back();
	}

}

void LevelComponent::LoadLevel(std::string file, std::vector<std::string> texturefiles)
{
	std::ifstream input(_data_path + file);

	std::vector<std::shared_ptr<svengine::Texture2D>> textures{};

	for (std::string texturefile : texturefiles)
	{
		textures.push_back(svengine::ResourceManager::GetInstance().LoadTexture(_data_path + texturefile));
	}

	int width{}, height{};
	int x{}, y{};

	SDL_QueryTexture(textures[0]->GetSDLTexture(), NULL,NULL, &width, &height);


	if (input.is_open())
	{
		std::string levelString{}, tempString{};

		while (std::getline(input, tempString))
		{
			levelString += tempString + '\n';
		}
		std::shared_ptr<svengine::GameObject> block{};
		for (size_t i = 0; i < levelString.length(); i++)
		{
			if (levelString[i] != ','&&levelString[i] != '\n')
			{
				svengine::Block* tempBlock{ new svengine::Block{
					{}, textures[std::stoi(&levelString[i])],std::stoi(&levelString[i]),width,height} };

				switch (std::stoi(&levelString[i]))
				{
				case 4:
					tempBlock->layer = _blocks.back()->layer;
					tempBlock->IsDug = true;
					break;

				case 5:
					tempBlock->IsDug = true;
					tempBlock->layer = _blocks.back()->layer;
					_pooka_spawns.push_back(tempBlock);
					break;
				case 6:
					tempBlock->IsDug = true;
					tempBlock->layer = _blocks.back()->layer;
					_player_spawns.push_back(tempBlock);
					break;
				case 7:
					tempBlock->IsDug = true;
					tempBlock->layer = _blocks.back()->layer;
					_boulder_spawns.push_back(tempBlock);
					break;
				case 8:
					tempBlock->IsDug = true;
					tempBlock->layer = _blocks.back()->layer;
					_fygar_spawns.push_back(tempBlock);

				}


				tempBlock->transform.SetPosition(float(x), float(y));
				x += width;
				_blocks.push_back(tempBlock);
			}
			else if(levelString[i]=='\n')
			{
				y += height;
				x = 0;
				_rows++;
			}
		}
		input.close();


		_columns = int(_blocks.size()) / _rows;
	}
}

void LevelComponent::Render(const svengine::Transform ) const
{
	for(auto block:_blocks)
	{
	
			svengine::Renderer::GetInstance().RenderTexture(block->texture->GetSDLTexture(), block->transform.GetPosition().x, block->transform.GetPosition().y,7,7);
		
	}
}

void LevelComponent::Dig(int idx)
{
	if (_blocks[idx]->layer > 0) {
		_blocks[idx]->texture = _dig_texture;
		_blocks[idx]->IsDug = true;
	}
}
