#include "MiniginPCH.h"
#include "LevelTrackComponent.h"


LevelTrackComponent::LevelTrackComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent> level)
	:Component(parent)
	,_level(level)
{
	_transform = &_parent.lock()->GetTransformRef();

	std::shared_ptr<PlayerTextureComponent> texture{ parent->GetComponent<PlayerTextureComponent>() };

	if (texture)
	{
		_width = texture->GetWidthAndHeight().first;
		_height = texture->GetWidthAndHeight().second;
	}
}


void LevelTrackComponent::Update()
{
	auto blocks{ _level.lock()->GetBlocks() };

	int side{ _level.lock()->GetBlockSize() };
	std::pair<int, int> Dimensions{ _level.lock()->GetRowsAndColumns() };

	_x = int(_transform->GetPosition().x);
	_y = int(_transform->GetPosition().y);

	int gridX{ _x / side }, gridY{ _y / side };

	_idx = gridX + gridY * Dimensions.second;

}
