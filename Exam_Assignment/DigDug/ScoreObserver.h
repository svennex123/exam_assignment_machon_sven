#pragma once
#include "Observer.h"
class ScoreObserver final:
	public svengine::Observer
{
public:
	ScoreObserver(std::weak_ptr<svengine::GameObject> scoreObject);


	void OnNotify(svengine::GameObject* object, svengine::Event event, svengine::ObserverArguments* argument) override;

	const int GetScore()const { return _score; }
	~ScoreObserver() = default;
	ScoreObserver(const ScoreObserver&) = default;
	ScoreObserver(ScoreObserver&&) = default;
	ScoreObserver& operator=(const ScoreObserver&) = default;
	ScoreObserver& operator=(ScoreObserver&&) = default;
private:
	int _score;
	std::weak_ptr<svengine::GameObject> _text_object;
};

