#pragma once
#include "PlayerStateComponent.h"
#include "MovementComponent.h"
#include "DigComponent.h"
#include "EnemyStateComponent.h"
#include "EnemyControlComponent.h"
#include "HoseComponent.h"

namespace svengine {
	class Command
	{
	public:
		Command() = default;
		virtual ~Command() = default;
		virtual void Execute(std::shared_ptr<GameObject> pobject) = 0;
	};

	//
	//LEFT COMMAND
	//
	class PlayerLeftCommand final :
		public Command
	{
	public:
		PlayerLeftCommand() = default;
		~PlayerLeftCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();
			if (state.get())
			{
				if (state->GetState() == PlayerState::idle)
				{
					state->SetState(PlayerState::moving_left);
					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveLeft();

					if (pobject->GetComponent<DigComponent>())
					{
						pobject->GetComponent<DigComponent>()->Dig();
					}
				}
			}

		}
	};

	//
	//RIGHT COMMAND
	//
	class PlayerRightCommand final :
		public Command
	{
	public:
		PlayerRightCommand() = default;
		~PlayerRightCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here

			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if (state->GetState() ==PlayerState::idle)
				{
					state->SetState(PlayerState::moving_right);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveRight();

					if (pobject->GetComponent<DigComponent>())
					{
						pobject->GetComponent<DigComponent>()->Dig();
					}

				}
			}
		}
	};

	//
	//UP COMMAND
	//
	class PlayerUpCommand final :
		public Command
	{
	public:
		PlayerUpCommand() = default;
		~PlayerUpCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if (state->GetState() == PlayerState::idle)
				{
					state->SetState(PlayerState::moving_up);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveUp();

					if (pobject->GetComponent<DigComponent>())
					{
						pobject->GetComponent<DigComponent>()->Dig();
					}
				}
			}
		}
	};

	//
	//DOWN COMMAND
	//
	class PlayerDownCommand final :
		public Command
	{
	public:
		PlayerDownCommand() = default;
		~PlayerDownCommand() = default;
		void Execute(std::shared_ptr<GameObject> pobject) override {
			//put input effect here
			std::shared_ptr<PlayerStateComponent> state = pobject->GetComponent<PlayerStateComponent>();

			if (state.get())
			{
				if( state->GetState() == PlayerState::idle)
				{
					state->SetState(PlayerState::moving_down);

					if (pobject->GetComponent<MovementComponent>())
						pobject->GetComponent<MovementComponent>()->MoveDown();

					if (pobject->GetComponent<DigComponent>())
					{
						pobject->GetComponent<DigComponent>()->Dig();
					}
				}
			}
		}
	};


	class EnemyUpCommand final: public Command
	{
	public:
		EnemyUpCommand() = default;
		~EnemyUpCommand() = default;

		void Execute(std::shared_ptr<GameObject> pobject) override
		{
			std::shared_ptr<EnemyStateComponent> stateComp{ pobject->GetComponent<EnemyStateComponent>() };

			if (stateComp.get())
			{
				EnemyState state{ stateComp->GetState() };
				if (state!= EnemyState::pumped&&state != EnemyState::shooting)
				{
					if (pobject->GetComponent<MovementComponent>()->MoveUp())
					{
						if (!pobject->GetComponent<EnemyControlComponent>()->MovementWasValid())
						{

							pobject->GetComponent<MovementComponent>()->MoveDown();
							stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
						}
					}
					else { stateComp->SetState(svengine::EnemyState(rand() % 4 + 1)); }

				}
			}

		}
	};


	class EnemyDownCommand final:public Command
	{
	public:
		EnemyDownCommand() = default;
		~EnemyDownCommand() = default;

		void Execute(std::shared_ptr<GameObject> pobject) override
		{
			std::shared_ptr<EnemyStateComponent> stateComp{ pobject->GetComponent<EnemyStateComponent>() };

			if (stateComp.get())
			{
				EnemyState state{ stateComp->GetState() };
				if (state != EnemyState::pumped&&state != EnemyState::shooting)
				{
					if (pobject->GetComponent<MovementComponent>()->MoveDown())
					{
						if (!pobject->GetComponent<EnemyControlComponent>()->MovementWasValid())
						{
							pobject->GetComponent<MovementComponent>()->MoveUp();
							stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
						}
					}
					else
					{
						stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
					}

				}
			}
		}
	};

	class EnemyLeftCommand final: public Command
	{
	public:
		EnemyLeftCommand() = default;
		~EnemyLeftCommand() = default;

		void Execute(std::shared_ptr<GameObject> pobject) override
		{
			std::shared_ptr<EnemyStateComponent> stateComp{ pobject->GetComponent<EnemyStateComponent>() };

			if (stateComp.get())
			{
				EnemyState state{ stateComp->GetState() };
				if (state != EnemyState::pumped&&state != EnemyState::shooting)
				{
					if (pobject->GetComponent<MovementComponent>()->MoveLeft())
					{
						if (!pobject->GetComponent<EnemyControlComponent>()->MovementWasValid())
						{
							//stateComp->SetState(svengine::EnemyState::moving_right);
							pobject->GetComponent<MovementComponent>()->MoveRight();
							stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
						}
					}
					else
					{
						stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
					}

				}
			}
		}

	};

	class EnemyRightCommand final : public Command
	{
	public:
		EnemyRightCommand() = default;
		~EnemyRightCommand() = default;

		void Execute(std::shared_ptr<GameObject> pobject) override
		{
			std::shared_ptr<EnemyStateComponent> stateComp{ pobject->GetComponent<EnemyStateComponent>() };

			if (stateComp.get())
			{
				EnemyState state{ stateComp->GetState() };
				if (state != EnemyState::pumped&&state != EnemyState::shooting)
				{
					if (pobject->GetComponent<MovementComponent>()->MoveRight())
					{
						if (!pobject->GetComponent<EnemyControlComponent>()->MovementWasValid())
						{
							pobject->GetComponent<MovementComponent>()->MoveLeft();
							stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
							//stateComp->SetState(svengine::EnemyState::moving_left);
						}
					}
					else
					{
						stateComp->SetState(svengine::EnemyState(rand() % 4 + 1));
					}
				}
			}
		}

	};

	class ShootCommand final:public Command
	{
	public:
		ShootCommand() = default;
		~ShootCommand() = default;

		void Execute(std::shared_ptr<GameObject> pobject) override
		{
			if(pobject->GetComponent<HoseComponent>()->Shoot(pobject->GetComponent<PlayerTextureComponent>()->GetCurrentState()))
			pobject->GetComponent<PlayerStateComponent>()->SetState(PlayerState::pumping);

		}
	};

}

