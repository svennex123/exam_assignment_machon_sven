#pragma once
#include "Component.h"
class FygarSpawnComponent final :
	public svengine::Component
{
public:
	FygarSpawnComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level, svengine::Scene* scene, std::shared_ptr<svengine::Observer> observer);
	
	void Spawn();
	~FygarSpawnComponent() = default;
	FygarSpawnComponent(const FygarSpawnComponent&) = default;
	FygarSpawnComponent(FygarSpawnComponent&&) = default;
	FygarSpawnComponent& operator=(const FygarSpawnComponent&) = default;
	FygarSpawnComponent& operator=(FygarSpawnComponent&&) = default;

private:
	svengine::Scene * _scene;
	std::weak_ptr<LevelComponent> _level;
	std::shared_ptr<svengine::Observer> _observer;
};

