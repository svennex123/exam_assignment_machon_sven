#pragma once
#include "Transform.h"
#include "Texture2D.h"
namespace svengine {
	enum class PlayerState
	{
		idle,
		moving_up,
		moving_down,
		moving_right,
		moving_left,
		pumping
	};


	enum class EnemyState
	{
		idle,
		moving_up,
		moving_down,
		moving_left,
		moving_right,
		pumped,
		shooting
	};

	enum class InputEnum
	{
		button_A,
		button_B,
		button_X,
		button_Y,
		key_down,
		key_left,
		key_up,
		key_right,
		joystick_down,
		joystick_up,
		joystick_left,
		joystick_right,
		key_Q,
		key_W,
		key_A,
		key_S,
		key_D,
		key_slash
	};

	enum class Event
	{
		move_left,
		move_up,
		move_down,
		move_right,
		got_killed


	};
	
struct Block
{
	svengine::Transform transform;
	std::shared_ptr<svengine::Texture2D> texture;
	int layer;
	int width, height;
	bool IsDug = false;
};

}