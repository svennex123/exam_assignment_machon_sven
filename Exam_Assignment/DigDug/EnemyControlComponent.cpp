#include "MiniginPCH.h"
#include "EnemyControlComponent.h"
#include "EnemyStateComponent.h"
#include "TextureComponent.h"
#include "EnemyLifeComponent.h"
#include "FireBreathComponent.h"


EnemyControlComponent::EnemyControlComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent>level)
	:Component(parent)
	,_level(level)
{
	_transform = &_parent.lock()->GetTransformRef();

	std::shared_ptr<svengine::TextureComponent> texture{ parent->GetComponent<svengine::TextureComponent>() };

	if (texture)
	{
		_width = texture->GetWidthAndHeight().first;
		_height = texture->GetWidthAndHeight().second;
	}

}



void EnemyControlComponent::Update()
{
	svengine::EnemyState state{ _parent.lock()->GetComponent<EnemyStateComponent>()->GetState() };


	if(state!=svengine::EnemyState::pumped&&state!=svengine::EnemyState::shooting)
	{
		if (state==svengine::EnemyState::moving_right||state==svengine::EnemyState::moving_left)
		{
			if(rand()%100==1)
				if (_parent.lock()->GetComponent<FireBreathComponent>())
				{
					_parent.lock()->GetComponent<FireBreathComponent>()->Shoot(state);
				}
				else
				{
					_commands[state]->Execute(_parent.lock());
				}
			else
			{
				_commands[state]->Execute(_parent.lock());
			}
		}
		else
		{
		_commands[state]->Execute(_parent.lock());
		}
			

	}
	else if(!_parent.lock()->GetComponent<EnemyLifeComponent>()->GetIsPumped()&&state!=svengine::EnemyState::shooting)
	{
		_parent.lock()->GetComponent<EnemyStateComponent>()->SetState(svengine::EnemyState::moving_right);
	}

}

void EnemyControlComponent::AddCommand(svengine::EnemyState state, std::shared_ptr<svengine::Command> command)
{
	_commands.insert(std::pair<svengine::EnemyState, std::shared_ptr<svengine::Command>>{state,command});
}

bool EnemyControlComponent::MovementWasValid()
{
	auto blocks{ _level.lock()->GetBlocks() };

	_x = int(_transform->GetPosition().x);
	_y = int(_transform->GetPosition().y);

	int side{ _level.lock()->GetBlockSize() };
	std::pair<int, int> Dimensions{ _level.lock()->GetRowsAndColumns() };

	int gridX{int(_parent.lock()->GetTransformRef().GetPosition().x) / side }, gridY{int( _parent.lock()->GetTransformRef().GetPosition().y / side) };

	if (!MovementHelper(blocks, gridX + gridY * Dimensions.second))
		return false;
	if (!MovementHelper(blocks, gridX + (gridY + 1)*Dimensions.second))
		return false;
	if (!MovementHelper(blocks, gridX + (gridY - 1)*Dimensions.second))
		return false;
	
	if (!MovementHelper(blocks, (gridX + 1) + gridY * Dimensions.second))
		return false;

	if (!MovementHelper(blocks, (gridX + 1) + (gridY + 1)*Dimensions.second))
		return false;
	
	if (!MovementHelper(blocks, (gridX + 1) + (gridY - 1)*Dimensions.second))
		return false;
	
	if (!MovementHelper(blocks, (gridX - 1) + gridY * Dimensions.second))
		return false;
	
	if (!MovementHelper(blocks, (gridX - 1) + (gridY + 1)*Dimensions.second))
		return false;
	
	if (!MovementHelper(blocks, (gridX - 1) + (gridY - 1)*Dimensions.second))
		return false;

	return true;
}

bool EnemyControlComponent::MovementHelper(std::vector<svengine::Block*> blocks, int idx)
{
	std::pair<int, int> Dimensions{ _level.lock()->GetRowsAndColumns() };

	if ((idx >= 0 && idx < Dimensions.first*Dimensions.second) )
	{

		auto blockPosition{ blocks[idx]->transform.GetPosition() };
		if (_x <= blockPosition.x + blocks[idx]->width &&
			_x + _width >= blockPosition.x&&
			_y + _height >= blockPosition.y  &&
			_y <= blockPosition.y + blocks[idx]->height&&
			blocks[idx]->IsDug == false)

			return false;


		return true;
	
	}

	return true;
}



