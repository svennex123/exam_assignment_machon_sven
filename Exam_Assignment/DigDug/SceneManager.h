#pragma once
#include "Singleton.h"

namespace svengine
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene & CreateScene(const std::string& name);

		void RemoveScene()
		{
			size_t size{ mScenes.size() };
			for (size_t i = 0; i < size; i++)
			{
				mScenes.pop_back();
			}
			mActiveScene = 0;
		}

		void NextScene()
		{
			if (mActiveScene+1<=int(mScenes.size())-1)
			{
			mActiveScene++;
			}
		};

		void Update();
		void FixedUpdate();
		void Render();


		Scene& GetActiveScene() { return *mScenes[mActiveScene]; }

	private:
		std::vector<std::shared_ptr<Scene>> mScenes;
		int mActiveScene{0};
	};

}
