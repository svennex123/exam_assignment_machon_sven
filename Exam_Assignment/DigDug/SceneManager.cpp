#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"


void svengine::SceneManager::Update()
{
	mScenes[mActiveScene]->Update();
}

void svengine::SceneManager::FixedUpdate()
{
	mScenes[mActiveScene]->FixedUpdate();
}

void svengine::SceneManager::Render()
{
	mScenes[mActiveScene]->Render();

}

svengine::Scene& svengine::SceneManager::CreateScene(const std::string& name)
{
	const auto scene = std::shared_ptr<Scene>(new Scene(name));
	mScenes.push_back(scene);
	return *scene;
}
