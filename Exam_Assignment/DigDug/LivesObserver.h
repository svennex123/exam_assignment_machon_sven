#pragma once
#include "Observer.h"
class LivesObserver final :
	public svengine::Observer
{
public:
	LivesObserver(std::weak_ptr<svengine::GameObject> display);


	void OnNotify(svengine::GameObject* object, svengine::Event event, svengine::ObserverArguments* argument) override;

	~LivesObserver() = default;
	LivesObserver(const LivesObserver&) = default;
	LivesObserver(LivesObserver&&) = default;
	LivesObserver& operator=(const LivesObserver&) = default;
	LivesObserver& operator=(LivesObserver&&) = default;
private:
	std::weak_ptr<svengine::GameObject> _display;
};

