#include "MiniginPCH.h"
#include "HoseComponent.h"
#include "GameObject.h"
#include "TextureComponent.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "PlayerStateComponent.h"
#include "LevelTrackComponent.h"
#include "PlayerEnemyInteractComponent.h"
#include "EnemyLifeComponent.h"


HoseComponent::HoseComponent(std::shared_ptr<svengine::GameObject> parent)
	:Component(parent)
	,_current_state(svengine::PlayerState::moving_right)
{
	_textures.insert(
		std::pair<svengine::PlayerState,std::shared_ptr<svengine::Texture2D>>
		(svengine::PlayerState::moving_up, svengine::ResourceManager::GetInstance().LoadTexture("hose_up.png")));

	_textures.insert(
		std::pair<svengine::PlayerState, std::shared_ptr<svengine::Texture2D>>
		(svengine::PlayerState::moving_down, svengine::ResourceManager::GetInstance().LoadTexture("hose_down.png")));

	_textures.insert(
		std::pair<svengine::PlayerState, std::shared_ptr<svengine::Texture2D>>
		(svengine::PlayerState::moving_right, svengine::ResourceManager::GetInstance().LoadTexture("hose_right.png")));

	_textures.insert(
		std::pair<svengine::PlayerState, std::shared_ptr<svengine::Texture2D>>
		(svengine::PlayerState::moving_left, svengine::ResourceManager::GetInstance().LoadTexture("hose_left.png")));
	SetActive(false);
}

void HoseComponent::Update()
{
	_current_time--;

	auto hits=_parent.lock()->GetComponent<PlayerEnemyInteractComponent>()->GetCollidersInArea(_transform,_width,_height);

	for(auto hit:hits)
	{
		hit.lock()->GetComponent<EnemyLifeComponent>()->Pump();
	}




	if (_current_time<=0)
	{
		SetActive(false);
		_parent.lock()->GetComponent<svengine::PlayerStateComponent>()->SetState(svengine::PlayerState::idle);
	}
}

void HoseComponent::Render(const svengine::Transform ) const
{
	if (_textures.at(_current_state) != nullptr) {
		const auto pos = _transform.GetPosition();
		svengine::Renderer::GetInstance().RenderTexture(
			_textures.at(_current_state)->GetSDLTexture(), pos.x, pos.y);
	}
}

bool HoseComponent::Shoot(svengine::PlayerState state)
{
	
	auto levelTracker{ _parent.lock()->GetComponent<LevelTrackComponent>()};
	int idx = levelTracker->GetGridIndex();

	auto blocks{ levelTracker->GetBlocks() };

	auto dimensions=levelTracker->GetRowsAndColumns();

	

	SDL_QueryTexture(_textures.at(state)->GetSDLTexture(), NULL, NULL, &_width, &_height);


		bool isPossible{ true };

	switch (state)
	{
	case svengine::PlayerState::moving_right:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x+14 , _parent.lock()->GetTransformRef().GetPosition().y);
		for (int i{};i<7;i++)
		{
			if(!blocks[idx+i]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx+i-2]->IsDug)
			{
				isPossible = true;
			}
		}

		break;

	case svengine::PlayerState::moving_up:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x, _parent.lock()->GetTransformRef().GetPosition().y-30);
		for (int i{}; i < 6; i++)
		{
			if (!blocks[idx - (i*dimensions.second)]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx -( (i-2)*dimensions.second)]->IsDug)
			{
				isPossible = true;
			}
		}
		break;

	case svengine::PlayerState::moving_down:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x, _parent.lock()->GetTransformRef().GetPosition().y+7);
		for (int i{}; i < 7; i++)
		{
			if (!blocks[idx + (i*dimensions.second)]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx + ((i - 2)*dimensions.second)]->IsDug)
			{
				isPossible = true;
			}
		}


		break;

	case svengine::PlayerState::moving_left:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x -30, _parent.lock()->GetTransformRef().GetPosition().y);
		for (int i{}; i < 5; i++)
		{
			if (!blocks[idx - i]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx - (i - 2)]->IsDug)
			{
				isPossible = true;
			}
		}
		break;
	}


	if (isPossible)
	{
		SetActive(true);
		_current_state = state;
	}

	return isPossible;
}

void HoseComponent::SetActive(bool isactive )
{
	_is_active = isactive;

	_current_time = 20;

}
