#include "MiniginPCH.h"
#include "InputComponent.h"
namespace svengine
{
	InputComponent::InputComponent(std::shared_ptr<GameObject> parent)
		: Component(parent)
	{
	}

	void InputComponent::Update()
	{
		HandleInput();
	}

	void InputComponent::HandleInput()
	{
		bool GotInput(false);
		InputManager & inputManager{ InputManager::GetInstance() };
		if(_parent.lock()->GetComponent<PlayerStateComponent>()->GetState()!= PlayerState::pumping)
			_parent.lock()->GetComponent<PlayerStateComponent>()->SetState(PlayerState::idle);
		for (auto command : _commands)
		{
			if (inputManager.IsPressed(command.first))
			{
				command.second->Execute(_parent.lock());
				GotInput = true;
			}
		}


	}

	void InputComponent::AddCommand(InputEnum key, std::shared_ptr<Command> command)
	{
		_commands.insert(std::pair<InputEnum, std::shared_ptr<Command>>(key, command));
	}
}
