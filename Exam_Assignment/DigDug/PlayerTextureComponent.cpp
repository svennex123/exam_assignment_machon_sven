#include "MiniginPCH.h"
#include "PlayerTextureComponent.h"
#include "Texture2D.h"
#include "Enums.h"
#include "ResourceManager.h"

PlayerTextureComponent::PlayerTextureComponent(std::shared_ptr<svengine::GameObject> parent)
	:svengine::Component(parent)
	,_textures{}
	,_current_state(svengine::PlayerState::moving_right)
{
}

void PlayerTextureComponent::Update()
{
	svengine::PlayerState state = _parent.lock()->GetComponent<svengine::PlayerStateComponent>()->GetState();
	if (state != svengine::PlayerState::idle&&state!=svengine::PlayerState::pumping)
	{
		_current_state = state;
	}
}

void PlayerTextureComponent::Render(const svengine::Transform transform) const
{
	
	if (_textures.at(_current_state) != nullptr) {
		const auto pos = transform.GetPosition();
		svengine::Renderer::GetInstance().RenderTexture(
			_textures.at(_current_state)->GetSDLTexture(), pos.x, pos.y);
	}
}

void PlayerTextureComponent::AddTexture(svengine::PlayerState state, std::string filename)
{
	_textures.insert(std::pair<svengine::PlayerState, std::shared_ptr<svengine::Texture2D>>
		(state, svengine::ResourceManager::GetInstance().LoadTexture(filename)));
}

std::pair<int, int> PlayerTextureComponent::GetWidthAndHeight() const
{
	int width, height;


	SDL_QueryTexture(_textures.at(_current_state)->GetSDLTexture(), NULL, NULL, &width, &height);


	return std::pair<int,int>(width, height);

}


