#pragma once
#include "Component.h"
class FireBreathComponent final :
	public svengine::Component
{
public:
	FireBreathComponent(std::shared_ptr<svengine::GameObject> parent);

	void Update() override;
	void Render(const svengine::Transform) const override;
	bool Shoot(svengine::EnemyState state);
	void SetActive(bool active) override;

	~FireBreathComponent() = default;
	FireBreathComponent(const FireBreathComponent&) = default;
	FireBreathComponent(FireBreathComponent&&) = default;
	FireBreathComponent& operator=(const FireBreathComponent&) = default;
	FireBreathComponent& operator=(FireBreathComponent&&) = default;

private:
	svengine::Transform _transform;
	float _life_time;
	float _current_time;
	int _width, _height;
	std::map < svengine::EnemyState, std::shared_ptr<svengine::Texture2D>> _textures;
	svengine::EnemyState _current_state;
	bool _is_first_time{ true };
};

