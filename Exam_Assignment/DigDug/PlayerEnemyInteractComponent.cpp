#include "MiniginPCH.h"
#include "PlayerEnemyInteractComponent.h"
#include "TextureComponent.h"
#include "PlayerTextureComponent.h"
#include "LivesComponent.h"
#include "SceneManager.h"


PlayerEnemyInteractComponent::PlayerEnemyInteractComponent(std::shared_ptr<svengine::GameObject>parent)
	:Component(parent)
	,_transform(&parent->GetTransformRef())
{
}


void PlayerEnemyInteractComponent::AddColliders(std::weak_ptr<svengine::GameObject> collider)
{
	_colliders.push_back(collider);

}

void PlayerEnemyInteractComponent::Update()
{


	int amntKilled{};

	for (auto collider: _colliders)
	{
		if (collider.lock()->GetComponent<svengine::TextureComponent>()->GetActive()&&collider.lock()->GetComponent<EnemyStateComponent>()->GetState()!=svengine::EnemyState::pumped)
		{
			glm::vec2 colliderPos{ collider.lock()->GetTransformRef().GetPosition() };
			int colliderSize = collider.lock()->GetComponent<svengine::TextureComponent>()->GetWidthAndHeight().first;

			glm::vec2 objectPos{ _transform->GetPosition() };
			int objectSize{ _parent.lock()->GetComponent<PlayerTextureComponent>()->GetWidthAndHeight().first };


			if (objectPos.x <= colliderPos.x + colliderSize &&
				objectPos.x + objectSize >= colliderPos.x&&
				objectPos.y + objectSize >= colliderPos.y&&
				objectPos.y <= colliderPos.y + colliderSize)
			{
				_parent.lock()->GetComponent<LivesComponent>()->OnHit();
			}
		}
		else if(!collider.lock()->GetComponent<EnemyControlComponent>()->GetActive())
		{
			amntKilled++;
		}

	}

	if (amntKilled==int(_colliders.size()))
	{
		svengine::SceneManager::GetInstance().NextScene();
	}
}

std::vector<std::weak_ptr<svengine::GameObject>> PlayerEnemyInteractComponent::GetCollidersInArea(svengine::Transform transform, int width,
	int height)
{

	std::vector < std::weak_ptr<svengine::GameObject>> foundColliders;
	for (auto collider : _colliders)
	{
		
		int colliderSize{};
			glm::vec2 colliderPos{ collider.lock()->GetTransformRef().GetPosition() };

		if (collider.lock()->GetComponent<svengine::TextureComponent>())
		{
			colliderSize = collider.lock()->GetComponent<svengine::TextureComponent>()->GetWidthAndHeight().first;
			
		}
		else
		{
			colliderSize = 14;
		}

			glm::vec2 objectPos{ transform.GetPosition() };



			if (objectPos.x <= colliderPos.x + colliderSize &&
				objectPos.x + width >= colliderPos.x&&
				objectPos.y + height >= colliderPos.y&&
				objectPos.y <= colliderPos.y + colliderSize)
			{
				foundColliders.push_back(collider);
			}
	
	}
		return foundColliders;
}
