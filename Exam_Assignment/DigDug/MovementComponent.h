#pragma once
namespace svengine {
	class MovementComponent final:
		public Component
	{
	public:
		MovementComponent(std::shared_ptr<GameObject> parent,int speed,int xLimit,int yLimit);
		bool MoveLeft()const;
		bool MoveRight()const;
		bool MoveUp()const;
		bool MoveDown()const;

		void Render(const Transform ) const override{};

		~MovementComponent() = default;
		MovementComponent(const MovementComponent&) = default;
		MovementComponent(MovementComponent&&) = default;
		MovementComponent& operator=(const MovementComponent&) = default;
		MovementComponent& operator =(MovementComponent&&) = default;

	private:
		Transform * _transform;
		int _speed;
		int _x_limit,_y_limit;

	};

}