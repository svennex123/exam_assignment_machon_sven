#pragma once
#include "Transform.h"
namespace svengine {
	//Base class for all components
	//It has virtual functions and holds a bool to see if the object is active

	class  GameObject;

	class Component
	{
	public:
		inline Component(std::shared_ptr<GameObject>parent);
		virtual void Update() {};
		virtual void FixedUpdate() {};
		virtual void Render(const Transform )const {};

		virtual void SetActive(bool active) { _is_active = active; }
		bool GetActive() const{ return _is_active; }

		virtual ~Component() = default;
		Component(const Component&) = default;
		Component(Component&&) = default;
		Component& operator=(const Component&) = default;
		Component& operator=(Component&&) = default;

	protected:
		bool _is_active;
		std::weak_ptr<GameObject> _parent;
	};

		 Component::Component(std::shared_ptr<GameObject>parent)
		:_is_active(true)
		,_parent(parent)
	{
	}
}