#include "MiniginPCH.h"
#include "GravityComponent.h"


GravityComponent::GravityComponent(std::shared_ptr<svengine::GameObject> parent)
	:Component(parent)
	,_tracker(parent->GetComponent<LevelTrackComponent>())
	,_is_falling(false)
	,_transform(&parent->GetTransformRef())
{
}



void GravityComponent::Update()
{
	if (!_is_falling)
	{
		_is_falling = !HasFooting();
	}
	else
	{
		_transform->SetPosition(_transform->GetPosition().x, _transform->GetPosition().y + 3);
	}

}

bool GravityComponent::HasFooting()const
{
	int currentidx{ _tracker.lock()->GetGridIndex() };
	std::vector<svengine::Block*> blocks{ _tracker.lock()->GetBlocks() };
	std::pair<int, int> dimensions{ _tracker.lock()->GetRowsAndColumns() };

	//bool hasFooting{true};

	return !blocks[currentidx + 2 * dimensions.second]->IsDug || !blocks[currentidx + 1 + (2 * dimensions.second)]->IsDug;
}
