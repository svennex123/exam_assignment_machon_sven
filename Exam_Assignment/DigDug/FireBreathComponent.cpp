#include "MiniginPCH.h"
#include "FireBreathComponent.h"
#include "Texture2D.h"
#include "EnemyLifeComponent.h"
#include "PlayerEnemyInteractComponent.h"
#include "LivesComponent.h"
#include "LevelTrackComponent.h"
#include "SceneManager.h"
#include "Scene.h"


FireBreathComponent::FireBreathComponent(std::shared_ptr<svengine::GameObject> parent)
	:Component(parent)
	, _current_state(svengine::EnemyState::moving_right)
{
	_textures.insert(std::pair<svengine::EnemyState, std::shared_ptr<svengine::Texture2D>>(
		svengine::EnemyState::moving_right, svengine::ResourceManager::GetInstance().LoadTexture("fygar_fire_right.png")));

	_textures.insert(std::pair<svengine::EnemyState, std::shared_ptr<svengine::Texture2D>>(
		svengine::EnemyState::moving_left, svengine::ResourceManager::GetInstance().LoadTexture("fygar_fire_left.png")));


	SetActive(false);
}


void FireBreathComponent::Update()
{
	_current_time--;

	auto hits = _parent.lock()->GetComponent<PlayerEnemyInteractComponent>()->GetCollidersInArea(_transform, _width, _height);

	for (auto hit : hits)
	{
		hit.lock()->GetComponent<LivesComponent>()->OnHit();
	}




	if (_current_time <= 0)
	{
		SetActive(false);
		_parent.lock()->GetComponent<EnemyStateComponent>()->SetState(svengine::EnemyState::moving_right);
	}
}

void FireBreathComponent::Render(const svengine::Transform) const
{
	if (_textures.at(_current_state) != nullptr) {
		const auto pos = _transform.GetPosition();
		svengine::Renderer::GetInstance().RenderTexture(
			_textures.at(_current_state)->GetSDLTexture(), pos.x, pos.y);
	}
}

bool FireBreathComponent::Shoot(svengine::EnemyState state)
{
	if (_is_first_time)
	{
		auto colliders(svengine::SceneManager::GetInstance().GetActiveScene().GetObjectsOfName("DigDug"));
		
			for (auto collider : colliders)
				_parent.lock()->GetComponent<PlayerEnemyInteractComponent>()->AddColliders(collider);
		_is_first_time = false;
	}





	auto levelTracker{ _parent.lock()->GetComponent<LevelTrackComponent>() };
	int idx = levelTracker->GetGridIndex();

	auto blocks{ levelTracker->GetBlocks() };

	auto dimensions = levelTracker->GetRowsAndColumns();



	SDL_QueryTexture(_textures.at(state)->GetSDLTexture(), NULL, NULL, &_width, &_height);


	bool isPossible{ true };

	switch (state)
	{
	case svengine::EnemyState::moving_right:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x + 14, _parent.lock()->GetTransformRef().GetPosition().y);
		for (int i{}; i < 10; i++)
		{
			if (!blocks[idx + i]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx + i - 2]->IsDug)
			{
				isPossible = true;
			}
		}

		break;

	
	case svengine::EnemyState::moving_left:
		_transform.SetPosition(_parent.lock()->GetTransformRef().GetPosition().x - 50, _parent.lock()->GetTransformRef().GetPosition().y);
		for (int i{}; i < 8; i++)
		{
			if (!blocks[idx - i]->IsDug)
			{
				isPossible = false;
			}
			else if (!isPossible&&blocks[idx - (i - 2)]->IsDug)
			{
				isPossible = true;
			}
		}
		break;
	}


	if (isPossible)
	{
		SetActive(true);
		_current_state = state;
	}

	return isPossible;
}

void FireBreathComponent::SetActive(bool isactive)
{
	_is_active = isactive;
	 
	_current_time = 20;

}
