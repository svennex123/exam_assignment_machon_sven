#pragma once
#include "Component.h"
class Scene;
class PookaSpawnerComponent final :
	public svengine::Component
{
public:
	PookaSpawnerComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent> level,svengine::Scene* scene,std::shared_ptr<svengine::Observer> observer);
	

	~PookaSpawnerComponent() = default;
	PookaSpawnerComponent(const PookaSpawnerComponent&) = default;
	PookaSpawnerComponent(PookaSpawnerComponent&&) = default;
	PookaSpawnerComponent& operator=(const PookaSpawnerComponent&) = default;
	PookaSpawnerComponent& operator=(PookaSpawnerComponent&&) = default;

	void Spawn();

private:
	svengine::Scene * _scene;
	std::weak_ptr<LevelComponent> _level;
	std::shared_ptr<svengine::Observer> _observer;



};

