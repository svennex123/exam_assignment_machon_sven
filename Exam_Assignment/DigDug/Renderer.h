#pragma once
#include "Singleton.h"
#include <SDL.h>

struct SDL_Window;
struct SDL_Renderer;

namespace svengine
{
	class Texture2D;
	class Renderer final : public Singleton<Renderer>
	{
		SDL_Renderer* mRenderer = nullptr;

	public:
		void Init(SDL_Window* window,int x,int y);
		void Render();
		void Destroy();

		void RenderTexture( SDL_Texture *texture, float x, float y) const;
		void RenderTexture( SDL_Texture  *texture, float x, float y, float width, float height) const;

		SDL_Renderer* GetSDLRenderer() const { return mRenderer; }

	private:
		int _x_limit;
		int _y_limit;
	};
}

