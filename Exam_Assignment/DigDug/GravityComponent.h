#pragma once
#include "Component.h"
#include "LevelTrackComponent.h"

class GravityComponent final:
	public svengine::Component
{
public:
	GravityComponent(std::shared_ptr<svengine::GameObject> parent);

	void Update() override;

	~GravityComponent() = default;
	GravityComponent(const GravityComponent&) = default;
	GravityComponent(GravityComponent&&) = default;
	GravityComponent& operator=(const GravityComponent&) = default;
	GravityComponent& operator=(GravityComponent&&) = default;
private:
	std::weak_ptr<LevelTrackComponent> _tracker;
	bool _is_falling;
	svengine::Transform *_transform;


	bool HasFooting()const;

};

