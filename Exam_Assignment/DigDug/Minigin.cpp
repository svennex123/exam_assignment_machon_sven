#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include <SDL.h>
#include "Scene.h"
#include "FpsComponent.h"
#include "TextComponent.h"
#include "LevelComponent.h"
#include "PookaSpawnerComponent.h"
#include "LivesComponent.h"
#include "BoulderSpawnComponent.h"
#include "PlayerSpawnComponent.h"
#include "ScoreObserver.h"
#include "LivesObserver.h"
#include "TextureListComponent.h"
#include "FygarSpawnComponent.h"

//#define SOLO
#define CO_OP

void svengine::Minigin::Initialize()
{

	_window_width= 406;
	_window_height = 602;

	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		_window_width,
		_window_height,
		SDL_WINDOW_OPENGL
	);
	if (window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(window,_window_width,_window_height);
}

/**
 * Code constructing the scene world starts here
 */
void svengine::Minigin::LoadGame() const
{
#ifdef SOLO
	LoadLevel1();
	LoadLevel2();
#endif

#ifdef CO_OP
	LoadCoOp1();
	LoadCoOp2();
#endif


}

void svengine::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();
}

void svengine::Minigin::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();
	{
		auto tPrevious = std::chrono::high_resolution_clock::now();
		float lag = 0.f;
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();

		bool doContinue = true;
		while (doContinue)
		{
			auto tCurrent = std::chrono::high_resolution_clock::now();
			auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(tCurrent - tPrevious).count();
			tPrevious = tCurrent;
			lag += elapsedTime;

			doContinue = input.ProcessInput();

			while (lag >= msPerFrame)
			{
				sceneManager.Update();
				lag -= msPerFrame;
			}

			sceneManager.FixedUpdate();
			renderer.Render();

			if(input.NeedsRestart())
			{
				sceneManager.RemoveScene();
				LoadGame();

			}

		}	
	}

	Cleanup();
}

void svengine::Minigin::LoadLevel1() const
{
	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto& scene = SceneManager::GetInstance().CreateScene("Level_1");

	//base level
	std::shared_ptr<GameObject> level{ std::make_shared<GameObject>("level") };
	auto levelComp{ std::make_shared<LevelComponent>(level,"../Data/") };
	levelComp->LoadLevel("Level_1.csv", std::vector<std::string>
	{"blue.png", "beige.png", "green.png", "orange.png", "black.png", "pooka_spawn.png","dig_dug_spawn.png","boulder_spawn.png","fygar_spawn.png"});
	level->AddComponent(levelComp);


	//Pooka spawning
	
	std::shared_ptr<GameObject> scoredText{ std::make_shared<GameObject>("score_ui") };


	scoredText->AddComponent(std::make_shared<TextComponent>(scoredText, "0", font));
	std::shared_ptr<ScoreObserver> scoreObserver{ std::make_shared<ScoreObserver>(scoredText) };
	scoredText->SetTransform(200, 0);


	std::shared_ptr<GameObject> LivesDisplay{ std::make_shared<GameObject>("lives_display") };
	std::shared_ptr<Texture2D> lifeTexture{(ResourceManager::GetInstance().LoadTexture("../Data/DigDugRight.png"))};

	std::shared_ptr<TextureListComponent> textures{ std::make_shared<TextureListComponent>(LivesDisplay) };

	textures->AddTexture(lifeTexture,0,588 );
	textures->AddTexture(lifeTexture, 14, 588);
	textures->AddTexture(lifeTexture, 28, 588);

	std::shared_ptr<LivesObserver> livesObserver{ std::make_shared<LivesObserver>(LivesDisplay) };
	LivesDisplay->AddComponent(textures);
	


	auto pookaSpawncomp{ std::make_shared<PookaSpawnerComponent>(level, levelComp, &scene,scoreObserver)};
	level->AddComponent(pookaSpawncomp);

	//boulder spawning
	auto boulderSpawnComp{ std::make_shared<BoulderSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(boulderSpawnComp);

	//player spawning
	auto playerSpawnComp{ std::make_shared<PlayerSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(playerSpawnComp);

	auto fygarSpawnComp{ std::make_shared<FygarSpawnComponent>(level,levelComp,&scene,scoreObserver) };
	level->AddComponent(fygarSpawnComp);
	scene.Add(level);
	scene.Add(scoredText);
	scene.Add(LivesDisplay);
	fygarSpawnComp->Spawn();

	pookaSpawncomp->Spawn();
	boulderSpawnComp->Spawn();
	playerSpawnComp->Spawn(livesObserver);



	//fps counter
	auto fpsObject = std::make_shared<GameObject>("fpsObject");
	std::shared_ptr<svengine::TextComponent> fps_text{ std::make_shared<svengine::TextComponent>(fpsObject,"test", font) };
	fpsObject->AddComponent(fps_text);
	fpsObject->AddComponent(std::make_shared<svengine::FpsComponent>(fpsObject));
	scene.Add(fpsObject);

	//dig dug player


	//scene.Add(DigDug);
}

void svengine::Minigin::LoadLevel2() const
{
	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto& scene = SceneManager::GetInstance().CreateScene("Level_2");

	//base level
	std::shared_ptr<GameObject> level{ std::make_shared<GameObject>("level") };
	auto levelComp{ std::make_shared<LevelComponent>(level,"../Data/") };
	levelComp->LoadLevel("Level_2.csv", std::vector<std::string>
	{"blue.png", "beige.png", "green.png", "orange.png", "black.png", "pooka_spawn.png", "dig_dug_spawn.png", "boulder_spawn.png", "fygar_spawn.png"});
	level->AddComponent(levelComp);


	//Pooka spawning

	std::shared_ptr<GameObject> scoredText{ std::make_shared<GameObject>("score_ui") };


	scoredText->AddComponent(std::make_shared<TextComponent>(scoredText, "0", font));
	std::shared_ptr<ScoreObserver> scoreObserver{ std::make_shared<ScoreObserver>(scoredText) };
	scoredText->SetTransform(200, 0);


	std::shared_ptr<GameObject> LivesDisplay{ std::make_shared<GameObject>("lives_display") };
	std::shared_ptr<Texture2D> lifeTexture{ (ResourceManager::GetInstance().LoadTexture("../Data/DigDugRight.png")) };

	std::shared_ptr<TextureListComponent> textures{ std::make_shared<TextureListComponent>(LivesDisplay) };

	textures->AddTexture(lifeTexture, 0, 588);
	textures->AddTexture(lifeTexture, 14, 588);
	textures->AddTexture(lifeTexture, 28, 588);

	std::shared_ptr<LivesObserver> livesObserver{ std::make_shared<LivesObserver>(LivesDisplay) };
	LivesDisplay->AddComponent(textures);



	auto pookaSpawncomp{ std::make_shared<PookaSpawnerComponent>(level, levelComp, &scene,scoreObserver) };
	level->AddComponent(pookaSpawncomp);

	//boulder spawning
	auto boulderSpawnComp{ std::make_shared<BoulderSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(boulderSpawnComp);

	//player spawning
	auto playerSpawnComp{ std::make_shared<PlayerSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(playerSpawnComp);

	auto fygarSpawnComp{ std::make_shared<FygarSpawnComponent>(level,levelComp,&scene,scoreObserver) };
	level->AddComponent(fygarSpawnComp);

	scene.Add(level);
	scene.Add(scoredText);
	scene.Add(LivesDisplay);
	fygarSpawnComp->Spawn();
	pookaSpawncomp->Spawn();
	boulderSpawnComp->Spawn();
	playerSpawnComp->Spawn(livesObserver);



	//fps counter
	auto fpsObject = std::make_shared<GameObject>("fpsObject");
	std::shared_ptr<svengine::TextComponent> fps_text{ std::make_shared<svengine::TextComponent>(fpsObject,"test", font) };
	fpsObject->AddComponent(fps_text);
	fpsObject->AddComponent(std::make_shared<svengine::FpsComponent>(fpsObject));
	scene.Add(fpsObject);

	//dig dug player


	//scene.Add(DigDug);
}

void svengine::Minigin::LoadCoOp1() const
{
	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto& scene = SceneManager::GetInstance().CreateScene("Level_1");

	//base level
	std::shared_ptr<GameObject> level{ std::make_shared<GameObject>("level") };
	auto levelComp{ std::make_shared<LevelComponent>(level,"../Data/") };
	levelComp->LoadLevel("Level_1_co-op.csv", std::vector<std::string>
	{"blue.png", "beige.png", "green.png", "orange.png", "black.png", "pooka_spawn.png", "dig_dug_spawn.png", "boulder_spawn.png", "fygar_spawn.png"});
	level->AddComponent(levelComp);


	//Pooka spawning

	std::shared_ptr<GameObject> scoredText{ std::make_shared<GameObject>("score_ui") };


	scoredText->AddComponent(std::make_shared<TextComponent>(scoredText, "0", font));
	std::shared_ptr<ScoreObserver> scoreObserver{ std::make_shared<ScoreObserver>(scoredText) };
	scoredText->SetTransform(200, 0);


	std::shared_ptr<GameObject> LivesDisplay{ std::make_shared<GameObject>("lives_display") };
	std::shared_ptr<Texture2D> lifeTexture{ (ResourceManager::GetInstance().LoadTexture("../Data/DigDugRight.png")) };

	std::shared_ptr<TextureListComponent> textures{ std::make_shared<TextureListComponent>(LivesDisplay) };

	textures->AddTexture(lifeTexture, 0, 588);
	textures->AddTexture(lifeTexture, 14, 588);
	textures->AddTexture(lifeTexture, 28, 588);
	textures->AddTexture(lifeTexture, 42, 588);
	textures->AddTexture(lifeTexture, 56, 588);
	textures->AddTexture(lifeTexture, 70, 588);

	std::shared_ptr<LivesObserver> livesObserver{ std::make_shared<LivesObserver>(LivesDisplay) };
	LivesDisplay->AddComponent(textures);



	auto pookaSpawncomp{ std::make_shared<PookaSpawnerComponent>(level, levelComp, &scene,scoreObserver) };
	level->AddComponent(pookaSpawncomp);

	//boulder spawning
	auto boulderSpawnComp{ std::make_shared<BoulderSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(boulderSpawnComp);

	//player spawning
	auto playerSpawnComp{ std::make_shared<PlayerSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(playerSpawnComp);

	auto fygarSpawnComp{ std::make_shared<FygarSpawnComponent>(level,levelComp,&scene,scoreObserver) };
	level->AddComponent(fygarSpawnComp);
	

	scene.Add(level);
	scene.Add(scoredText);
	scene.Add(LivesDisplay);
	fygarSpawnComp->Spawn();
	pookaSpawncomp->Spawn();
	boulderSpawnComp->Spawn();
	playerSpawnComp->Spawn(livesObserver);



	//fps counter
	auto fpsObject = std::make_shared<GameObject>("fpsObject");
	std::shared_ptr<svengine::TextComponent> fps_text{ std::make_shared<svengine::TextComponent>(fpsObject,"test", font) };
	fpsObject->AddComponent(fps_text);
	fpsObject->AddComponent(std::make_shared<svengine::FpsComponent>(fpsObject));
	scene.Add(fpsObject);

	//dig dug player


	//scene.Add(DigDug);
}

void svengine::Minigin::LoadCoOp2() const
{
	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto& scene = SceneManager::GetInstance().CreateScene("Level_2");

	//base level
	std::shared_ptr<GameObject> level{ std::make_shared<GameObject>("level") };
	auto levelComp{ std::make_shared<LevelComponent>(level,"../Data/") };
	levelComp->LoadLevel("Level_2_co-op.csv", std::vector<std::string>
	{"blue.png", "beige.png", "green.png", "orange.png", "black.png", "pooka_spawn.png", "dig_dug_spawn.png", "boulder_spawn.png", "fygar_spawn.png"});
	level->AddComponent(levelComp);


	//Pooka spawning

	std::shared_ptr<GameObject> scoredText{ std::make_shared<GameObject>("score_ui") };


	scoredText->AddComponent(std::make_shared<TextComponent>(scoredText, "0", font));
	std::shared_ptr<ScoreObserver> scoreObserver{ std::make_shared<ScoreObserver>(scoredText) };
	scoredText->SetTransform(200, 0);


	std::shared_ptr<GameObject> LivesDisplay{ std::make_shared<GameObject>("lives_display") };
	std::shared_ptr<Texture2D> lifeTexture{ (ResourceManager::GetInstance().LoadTexture("../Data/DigDugRight.png")) };

	std::shared_ptr<TextureListComponent> textures{ std::make_shared<TextureListComponent>(LivesDisplay) };

	textures->AddTexture(lifeTexture, 0, 588);
	textures->AddTexture(lifeTexture, 14, 588);
	textures->AddTexture(lifeTexture, 28, 588);
	textures->AddTexture(lifeTexture, 42, 588);
	textures->AddTexture(lifeTexture, 56, 588);
	textures->AddTexture(lifeTexture, 70, 588);

	std::shared_ptr<LivesObserver> livesObserver{ std::make_shared<LivesObserver>(LivesDisplay) };
	LivesDisplay->AddComponent(textures);



	auto pookaSpawncomp{ std::make_shared<PookaSpawnerComponent>(level, levelComp, &scene,scoreObserver) };
	level->AddComponent(pookaSpawncomp);

	//boulder spawning
	auto boulderSpawnComp{ std::make_shared<BoulderSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(boulderSpawnComp);

	//player spawning
	auto playerSpawnComp{ std::make_shared<PlayerSpawnComponent>(level,levelComp,&scene) };
	level->AddComponent(playerSpawnComp);

	auto fygarSpawnComp{ std::make_shared<FygarSpawnComponent>(level,levelComp,&scene,scoreObserver) };
	level->AddComponent(fygarSpawnComp);


	scene.Add(level);
	scene.Add(scoredText);
	scene.Add(LivesDisplay);
	fygarSpawnComp->Spawn();
	pookaSpawncomp->Spawn();
	boulderSpawnComp->Spawn();
	playerSpawnComp->Spawn(livesObserver);



	//fps counter
	auto fpsObject = std::make_shared<GameObject>("fpsObject");
	std::shared_ptr<svengine::TextComponent> fps_text{ std::make_shared<svengine::TextComponent>(fpsObject,"test", font) };
	fpsObject->AddComponent(fps_text);
	fpsObject->AddComponent(std::make_shared<svengine::FpsComponent>(fpsObject));
	scene.Add(fpsObject);


}
