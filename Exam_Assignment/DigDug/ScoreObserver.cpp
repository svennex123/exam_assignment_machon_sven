#include "MiniginPCH.h"
#include "ScoreObserver.h"
#include "LevelTrackComponent.h"
#include "TextComponent.h"


ScoreObserver::ScoreObserver(std::weak_ptr<svengine::GameObject> scoreObject)
	:_score(0)
	,_text_object(scoreObject)
	
{
}



void ScoreObserver::OnNotify(svengine::GameObject* object, svengine::Event event, svengine::ObserverArguments* )
{
	if(event==svengine::Event::got_killed)
	{
		int layer=object->GetComponent<LevelTrackComponent>()->GetBlocks()[object->GetComponent<LevelTrackComponent>()->GetGridIndex()]->layer;

		if (layer<=1)
		{
			if (object->GetName() == "pooka")
			{
				_score += 200;
			}
			else if (object->GetName() == "fygar")
			{
				_score += 400;
			}
		}
		else
		{
			if (object->GetName()=="pooka")
			{
			_score += 200 + ((layer - 1) * 100);
				
			}
			else if(object->GetName()=="fygar")
			{
				_score += 400 + ((layer - 1) * 200);
			}
		}

		_text_object.lock()->GetComponent<svengine::TextComponent>()->SetText("score:"+std::to_string(_score));
	}

}
