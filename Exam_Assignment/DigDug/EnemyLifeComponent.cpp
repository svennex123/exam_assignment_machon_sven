#include "MiniginPCH.h"
#include "EnemyLifeComponent.h"


EnemyLifeComponent::EnemyLifeComponent(std::shared_ptr<svengine::GameObject> parent)
	:Component(parent)
	,_max_pump_level(100)
	,_current_pump_level(0)
{
}


void EnemyLifeComponent::Update()
{
	if (!_is_pumped&&_current_pump_level>0)
	{
		_current_pump_level--;
	}
	else if(_is_pumped)
	{
		_current_pump_level++;
		if (_current_pump_level>=_max_pump_level)
		{
			svengine::ObserverArguments arg{};
			_parent.lock()->SetActive(false);
			_parent.lock()->Notify(svengine::Event::got_killed, &arg);

		}
	_is_pumped = false;
	}
}

void EnemyLifeComponent::Pump()
{
	_parent.lock()->GetComponent<EnemyStateComponent>()->SetState(svengine::EnemyState::pumped);
	_is_pumped = true;


}
