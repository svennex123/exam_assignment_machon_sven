#pragma once
#include "Component.h"
class EnemyLifeComponent final :
	public svengine::Component
{
public:
	EnemyLifeComponent(std::shared_ptr<svengine::GameObject>parent);
	void Update() override;
	void Pump();
	bool GetIsPumped()const { return _current_pump_level>0; }
	

	~EnemyLifeComponent()=default;
	EnemyLifeComponent(const EnemyLifeComponent&) = default;
	EnemyLifeComponent(EnemyLifeComponent&&) = default;
	EnemyLifeComponent& operator=(const EnemyLifeComponent&) = default;
	EnemyLifeComponent& operator=(EnemyLifeComponent&&) = default;



private:
	int _current_pump_level;
	int _max_pump_level;
	bool _is_pumped;


};

