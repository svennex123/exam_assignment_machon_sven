#pragma once
#include "Component.h"
class BoulderSpawnComponent final:
	public svengine::Component
{
public:
	BoulderSpawnComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent> level,svengine::Scene*scene);


	void Spawn();


	~BoulderSpawnComponent() = default;
	BoulderSpawnComponent(const BoulderSpawnComponent&) = default;
	BoulderSpawnComponent(BoulderSpawnComponent&&) = default;
	BoulderSpawnComponent& operator=(const BoulderSpawnComponent&) = default;
	BoulderSpawnComponent& operator=(BoulderSpawnComponent&&) = default;
private:
	std::weak_ptr<LevelComponent> _level;
	svengine::Scene* _scene;
};

