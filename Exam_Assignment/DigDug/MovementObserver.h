#pragma once
namespace svengine {
	class MovementObserver :
		public Observer
	//Example Observer, just breaks when moved a certain amount of units
	{
	public:
		MovementObserver();
		
		void OnNotify(GameObject* object, Event event, ObserverArguments * argument) override;

		~MovementObserver()=default;
		MovementObserver(const MovementObserver&) = default;
		MovementObserver(MovementObserver&&) = default;
		MovementObserver& operator=(const MovementObserver&) = default;
		MovementObserver& operator=(MovementObserver&&) = default;
	private:
		int _amount_moved;
	};
}
