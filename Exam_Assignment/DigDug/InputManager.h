#pragma once
#include <XInput.h>
#include "Singleton.h"
namespace svengine
{
	

	class InputManager final : public Singleton<InputManager>
	{
	public:
		bool ProcessInput();
		bool IsPressed(InputEnum input) const;
		bool NeedsRestart();
	private:

		XINPUT_STATE _current_State{};
		InputEnum _button;
	};

}
