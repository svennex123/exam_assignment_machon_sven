#include "MiniginPCH.h"
#include "MovementArguments.h"

svengine::MovementArguments::MovementArguments(int speed)
	:ObserverArguments(),_speed{speed}
{
}

const int svengine::MovementArguments::GetSpeed() const
{
	{ return _speed; }
}
