#include "MiniginPCH.h"
#include "FpsComponent.h"
#include "TextComponent.h"
svengine::FpsComponent::FpsComponent(std::shared_ptr<GameObject> parent)
	:_current_time(std::chrono::high_resolution_clock::now())
	, _previous_time(std::chrono::high_resolution_clock::now())
	, _ptext_component()
	, Component(parent)
{
	_ptext_component = _parent.lock()->GetComponent<TextComponent>();

}


void svengine::FpsComponent::FixedUpdate()
{
	_current_time = std::chrono::high_resolution_clock::now();
	const float elapsedSec{ (float)std::chrono::duration_cast<std::chrono::milliseconds>(_current_time - _previous_time).count() };
	_previous_time = _current_time;

	if (elapsedSec > 0 && _ptext_component != nullptr)_ptext_component->SetText(std::to_string(1000 / int(elapsedSec)));

}
