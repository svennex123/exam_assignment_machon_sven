#include "MiniginPCH.h"
#include "Texture2D.h"
#include "TextureComponent.h"
namespace svengine
{
	 TextureComponent::TextureComponent(std::shared_ptr<GameObject> parent, std::string filename)
		:Component(parent)
	{
		_texture = ResourceManager::GetInstance().LoadTexture(filename);

	}

	TextureComponent::TextureComponent(std::shared_ptr<GameObject> parent, std::shared_ptr<Texture2D> texture)
		:Component(parent)
		,_texture(texture)
	{
	}

	std::pair<int, int> TextureComponent::GetWidthAndHeight() const
	{
		int width, height;


		SDL_QueryTexture(_texture->GetSDLTexture(), NULL, NULL, &width, &height);


		return std::pair<int, int>(width, height);

	}

	void TextureComponent::Render(const Transform transform) const
	{
		if (_texture != nullptr)
		{
			const auto pos = transform.GetPosition();
			Renderer::GetInstance().RenderTexture(_texture->GetSDLTexture(), pos.x, pos.y);
		}
	}

}
