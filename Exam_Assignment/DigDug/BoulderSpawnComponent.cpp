#include "MiniginPCH.h"
#include "BoulderSpawnComponent.h"
#include "TextureComponent.h"
#include "Scene.h"
#include "LevelTrackComponent.h"
#include "GravityComponent.h"


BoulderSpawnComponent::BoulderSpawnComponent(std::shared_ptr<svengine::GameObject> parent,std::weak_ptr<LevelComponent>level, svengine::Scene*scene)
	:Component(parent)
	,_level(level)
	,_scene(scene)
{
}


void BoulderSpawnComponent::Spawn()
{
	auto spawnPoints{ _level.lock()->GetBoulderSpawns() };


	for (auto spawnpoint:spawnPoints)
	{
		std::shared_ptr<svengine::GameObject> boulder{ std::make_shared<svengine::GameObject>("boulder") };

		boulder->AddComponent(std::make_shared<svengine::TextureComponent>(boulder, "boulder.png"));
		boulder->SetTransform(spawnpoint->transform.GetPosition().x, spawnpoint->transform.GetPosition().y);
		boulder->AddComponent(std::make_shared<LevelTrackComponent>(boulder, _level));
		boulder->AddComponent(std::make_shared<GravityComponent>(boulder));
		_scene->Add(boulder);
	}
}
