#include "MiniginPCH.h"
#include "PookaSpawnerComponent.h"
#include "GameObject.h"
#include "TextureComponent.h"
#include "MovementComponent.h"
#include "EnemyControlComponent.h"
#include "EnemyLifeComponent.h"
#include "LevelTrackComponent.h"
#include "Scene.h"


PookaSpawnerComponent::PookaSpawnerComponent(std::shared_ptr<svengine::GameObject> parent, std::weak_ptr<LevelComponent> level,svengine::Scene* scene
	, std::shared_ptr<svengine::Observer> observer)
	:Component(parent)
	,_level(level)
	,_scene(scene)
	,_observer(observer)
{
}


void PookaSpawnerComponent::Spawn()
{
	std::vector<svengine::Block*> spawnPoints{ _level.lock()->GetPookaSpawns() };

	for(auto spawnPoint:spawnPoints)
	{
		std::shared_ptr<svengine::GameObject> pooka{std::make_shared<svengine::GameObject>("pooka")};
		pooka->AddComponent(std::make_shared<svengine::TextureComponent>(pooka,"pooka_idle.png"));


		pooka->AddComponent(std::make_shared<EnemyStateComponent>(pooka));

		pooka->AddComponent(
			std::make_shared<svengine::MovementComponent>
			(pooka, 2,_level.lock()->GetRowsAndColumns().second*_level.lock()->GetBlockSize(),_level.lock()->GetRowsAndColumns().first*_level.lock()->GetBlockSize()));

		std::shared_ptr<EnemyControlComponent> control_component{ std::make_shared<EnemyControlComponent>(pooka,_level) };
		control_component->AddCommand(svengine::EnemyState::moving_down, std::make_shared<svengine::EnemyDownCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_up, std::make_shared<svengine::EnemyUpCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_left, std::make_shared<svengine::EnemyLeftCommand>());
		control_component->AddCommand(svengine::EnemyState::moving_right, std::make_shared<svengine::EnemyRightCommand>());

		pooka->AddComponent(control_component);

		pooka->AddComponent(std::make_shared<EnemyLifeComponent>(pooka));

		pooka->AddComponent(std::make_shared<LevelTrackComponent>(pooka, _level));

		pooka->SetTransform(spawnPoint->transform.GetPosition().x,spawnPoint->transform.GetPosition().y);

		pooka->AddObserver(_observer);


		
		_scene->Add(pooka);
	}

}
