#pragma once
#include "Component.h"
#include <map>
#include "Texture2D.h"
#include "Enums.h"
#include "TextureComponent.h"

class PlayerTextureComponent final :
	public svengine::Component
{
public:
	PlayerTextureComponent(std::shared_ptr<svengine::GameObject> parent);
	
	void Update() override;
	void FixedUpdate() override {};
	void Render(const svengine::Transform transform) const override;
	void AddTexture(svengine::PlayerState state, std::string filename);
	std::pair<int, int> GetWidthAndHeight()const;
	svengine::PlayerState GetCurrentState()const { return _current_state; }


	~PlayerTextureComponent() = default;
	PlayerTextureComponent(const PlayerTextureComponent&) = default;
	PlayerTextureComponent(PlayerTextureComponent&&) = default;
	PlayerTextureComponent& operator=(const PlayerTextureComponent&) = default;
	PlayerTextureComponent& operator=(PlayerTextureComponent&&) = default;

private:
	std::map<svengine::PlayerState, std::shared_ptr<svengine::Texture2D>> _textures;
	svengine::PlayerState _current_state;
};

