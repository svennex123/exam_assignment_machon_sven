#include "MiniginPCH.h"
#include "LivesObserver.h"
#include "LivesComponent.h"
#include "TextureListComponent.h"


LivesObserver::LivesObserver(std::weak_ptr<svengine::GameObject> display)
	:_display(display)
{
}

void LivesObserver::OnNotify(svengine::GameObject* , svengine::Event event , svengine::ObserverArguments* )
{
	if (event==svengine::Event::got_killed)
	{
		_display.lock()->GetComponent<TextureListComponent>()->RemoveTexture();
	}

}
