#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>

#define DEAD_ZONE 15000  //deadzone value is configured to a ps4 controller, might result in weird controls with xbox
						 //do not have one to test

bool svengine::InputManager::ProcessInput()
{
	ZeroMemory(&_current_State, sizeof(XINPUT_STATE));
	XInputGetState(0, &_current_State);

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
		if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE:
			return false;
			break;

			}
		}
		if (e.type == SDL_MOUSEBUTTONDOWN) {
			
		}
	}

	return true;
}

bool svengine::InputManager::IsPressed(InputEnum input) const
{
	const Uint8 *keyState = SDL_GetKeyboardState(NULL);

	switch (input)
	{
	case InputEnum::button_A:
		return _current_State.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;
	case InputEnum::button_B:
		return _current_State.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case InputEnum::button_X:
		return _current_State.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case InputEnum::button_Y:
		return _current_State.Gamepad.wButtons & XINPUT_GAMEPAD_Y;

	case InputEnum::key_down:
		return keyState[SDL_SCANCODE_DOWN];
	case InputEnum::key_left:
		return keyState[SDL_SCANCODE_LEFT];
	case InputEnum::key_up:
		return keyState[SDL_SCANCODE_UP];
	case InputEnum::key_right:
		return keyState[SDL_SCANCODE_RIGHT];
	case InputEnum::key_Q:
		return keyState[SDL_SCANCODE_Q];

	case InputEnum::key_A:
		return keyState[SDL_SCANCODE_A];
	case InputEnum::key_W:
		return keyState[SDL_SCANCODE_W];
	case InputEnum::key_S:
		return keyState[SDL_SCANCODE_S];
	case InputEnum::key_D:
		return keyState[SDL_SCANCODE_D];
	case InputEnum::key_slash:
		return keyState[SDL_SCANCODE_SLASH];


	case InputEnum::joystick_down:
		return (_current_State.Gamepad.sThumbLY < 0 && _current_State.Gamepad.sThumbLY < -DEAD_ZONE);
	case InputEnum::joystick_up:
		return (_current_State.Gamepad.sThumbLY > 0 && _current_State.Gamepad.sThumbLY > DEAD_ZONE);
	case InputEnum::joystick_left:
		return (_current_State.Gamepad.sThumbLX < 0 && _current_State.Gamepad.sThumbLX < -DEAD_ZONE);
	case InputEnum::joystick_right:
		return (_current_State.Gamepad.sThumbLX > 0 && _current_State.Gamepad.sThumbLX > DEAD_ZONE);
	default: return false;
	}
}

bool svengine::InputManager::NeedsRestart()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym)
			{


			case SDLK_r:
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	return false;
}