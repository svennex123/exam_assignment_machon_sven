#pragma once
#include "Component.h"
class EnemyStateComponent final :
	public svengine::Component
{
public:
	EnemyStateComponent(std::shared_ptr<svengine::GameObject> parent);
	

	void SetState(svengine::EnemyState state) { _current_state = state; }
	const svengine::EnemyState GetState() { return _current_state; }

	~EnemyStateComponent() = default;
	EnemyStateComponent(const EnemyStateComponent&) = default;
	EnemyStateComponent(EnemyStateComponent&&) = default;
	EnemyStateComponent& operator=(const EnemyStateComponent&) = default;
	EnemyStateComponent& operator=(EnemyStateComponent&&) = default;



private:
	svengine::EnemyState _current_state;
};

