#pragma once
#include "Component.h"
#include <SDL.h>

#include "Font.h"
#include "Texture2D.h"

namespace svengine {
	class TextComponent final :
		public Component
	{
	public:
		 TextComponent(std::shared_ptr<GameObject> parent,std::string text, std::shared_ptr<Font>  font);
	
		 void SetText(std::string newText);
		 void Update()override;
		 void Render(const Transform transform)const override;
		 void SetColor(SDL_Color color);

		 ~TextComponent() = default;
		 TextComponent(const TextComponent&) = default;
		 TextComponent(TextComponent&&) = default;
		 TextComponent& operator=(const TextComponent&) = default;
		 TextComponent& operator=(TextComponent&&) = default;
		
	private:
		std::string _text;
		std::shared_ptr<Font> _font;
		bool _needs_update;
		SDL_Color _color;
		std::shared_ptr<Texture2D> _texture;
	};


	
}

