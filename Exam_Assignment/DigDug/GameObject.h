#pragma once
#include "SceneObject.h"
#include "Observer.h"
#include "algorithm"

//Base GameObject
//Holds vector with all components of the game object
// iterates over them all and if they're active updates/renders them

namespace svengine
{

	class GameObject :
		public SceneObject
	{
	public:
		GameObject(std::string name) :_components{}, _name{ name }{}
		~GameObject();

		void Update() override;
		void Render() const override;
		void FixedUpdate() override;


		void SetActive(bool isActive);

		void AddComponent(std::shared_ptr<Component>component) { _components.push_back(component); };
		template<class T> std::shared_ptr<T> GetComponent();


		void SetTransform(float x, float y) { _transform.SetPosition(x, y); }
		Transform& GetTransformRef() { return _transform; }

		void AddObserver(std::shared_ptr<Observer> obsrv);
		void RemoveObserver(std::shared_ptr<Observer>obsrv);
		void Notify(Event event, ObserverArguments * argument);

		std::string GetName()const { return _name; }


	private:

		std::vector<std::shared_ptr<Component>> _components;
		Transform _transform;
		std::vector<std::shared_ptr<Observer>> _observers;
		const std::string _name;

	};

	template <class T>
	std::shared_ptr<T> GameObject::GetComponent()
	{
		for(std::shared_ptr<Component> component:_components)
		{
			if (typeid(*component) == typeid(T))
				return std::static_pointer_cast<T>(component);
		}
		return nullptr;
	}


	inline GameObject::~GameObject()
	{
	}

	inline void GameObject::Update()
	{
		for (auto component : _components)
		{
			if(component->GetActive())
			component->Update();
		}
		
	}

	inline void GameObject::Render() const
	{
		for (auto component : _components)
		{
			if(component->GetActive())
			component->Render(_transform);
		}
	}

	inline void GameObject::FixedUpdate()
	{
		for (auto component : _components)
		{
			if (component->GetActive())
				component->FixedUpdate();
		}
	}

	inline void GameObject::SetActive(bool isActive)
	{

		for (auto component: _components)
		{
			component->SetActive(isActive);
		}
	}

	inline void GameObject::AddObserver(std::shared_ptr<Observer> obsrv)
	{
		if (std::find(_observers.begin(),_observers.end(),obsrv)==_observers.end())
		{
			_observers.push_back(obsrv);
		}
	}

	inline void GameObject::RemoveObserver(std::shared_ptr<Observer> obsrv)
	{
		_observers.erase(std::remove(_observers.begin(), _observers.end(), obsrv), _observers.end());
	}

	inline void GameObject::Notify(Event event, ObserverArguments * argument)
	{
		for (auto observer:_observers)
		{
			observer->OnNotify(this, event,  argument);
		}
	}
}
